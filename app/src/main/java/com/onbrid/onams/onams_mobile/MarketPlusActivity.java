package com.onbrid.onams.onams_mobile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.onbrid.onams.onams_mobile.util.ImageUtil;
import com.onbrid.onams.onams_mobile.util.OnbridPreference;
import com.onbrid.onams.onams_mobile.util.OnbridResponseHandler;
import com.onbrid.onams.onams_mobile.util.OnbridRestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;

public class MarketPlusActivity extends AppCompatActivity {

    private Toolbar toolbar;

    private Button btnScan ;
    private Button btnSearch ;
    private Button btnCamera ;

    private ImageView img;

    private EditText etAssetNo;
    private EditText etTelNo;
    private TextView tvAssetName;
    private TextView tvModelName;
    private TextView tvSpec;
    private TextView tvDeptName;
    private TextView tvPlaceName;
    private TextView tvRecordDate;

    private String deptNo;
    private String placeNo;

    private File mPath;
    private String mFileName;
    private static final String JPEG_FILE_PATH   = "ONBRID/MARKET";
    private static final String JPEG_FILE_PREFIX = "MK_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";

    private static final int CAMERA = 1;

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.btnScan :
                    new IntentIntegrator(MarketPlusActivity.this).initiateScan();
                    break;
                case R.id.btnSearch :
                    if (etAssetNo.getText().toString().trim().equalsIgnoreCase("")) {
                        Toast.makeText(MarketPlusActivity.this, "재물번호를 먼저 입력하세요", Toast.LENGTH_LONG).show();
                        return;
                    }
                    searchAssetNo(etAssetNo.getText().toString().trim());
                    break;
                case R.id.btnCamera :
                    String phoneNum = ((TelephonyManager) getApplicationContext().getSystemService(getApplicationContext().TELEPHONY_SERVICE)).getLine1Number();
                    mFileName = etAssetNo.getText().toString().trim() + "_" + phoneNum + "_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + JPEG_FILE_SUFFIX;

                    openCamera(mPath, mFileName);
                    break;
                case R.id.img : Log.e("img", "ImageView Click~!!");
                    break;
                default: break;
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_plus);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initControll();

        mPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/" + JPEG_FILE_PATH + "/" + new SimpleDateFormat("yyyyMMdd").format(new Date()));
        if (!mPath.exists()) mPath.mkdirs();


    }


    /**
     * 컨트롤 세팅 이벤트 연결
     */
    private void initControll() {

        btnScan   = (Button) findViewById(R.id.btnScan);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnCamera = (Button) findViewById(R.id.btnCamera);

        img = (ImageView) findViewById(R.id.img);


        etAssetNo = (EditText) findViewById(R.id.etAssetNo);
        etTelNo   = (EditText) findViewById(R.id.etTelNo);

        tvAssetName = (TextView) findViewById(R.id.tvAssetName);
        tvModelName = (TextView) findViewById(R.id.tvModelName);
        tvSpec      = (TextView) findViewById(R.id.tvSpec);
        tvDeptName  = (TextView) findViewById(R.id.tvDeptName);
        tvPlaceName = (TextView) findViewById(R.id.tvPlaceName);
        tvRecordDate = (TextView) findViewById(R.id.tvRecordDate);

        btnScan.setOnClickListener(clickListener);
        btnSearch.setOnClickListener(clickListener);
        btnCamera.setOnClickListener(clickListener);
        img.setOnClickListener(clickListener);

        clearControll();
    }

    /**
     * 컨트롤 초기화 한다.
     */
    private void clearControll() {
        etAssetNo.setText("");
        etTelNo.setText("");
        tvAssetName.setText("");
        tvModelName.setText("");
        tvSpec.setText("");
        tvDeptName.setText("");
        tvPlaceName.setText("");
        tvRecordDate.setText("");

        etAssetNo.setEnabled(true);
        etTelNo.setEnabled(false);
        btnCamera.setEnabled(false);

        img.setImageBitmap(null);
        mFileName = "";
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_market_plus, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_save :
                // 재고활용 상품 등록
                if (etTelNo.getText().toString().trim().length() < 1) {
                    Toast.makeText(this, "연락처를 입력하세요.", Toast.LENGTH_LONG).show();
                    return false;
                }
                insertMarketAsset();

                break;
            case R.id.action_clear :
                clearControll();
                break;
            default :
                finish();
                break;
        }



        return super.onOptionsItemSelected(item);
    }





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            Log.d("MainActivity", "result");

            if(result.getContents() == null) {
                Toast.makeText(this, "재물번호 바코드 스캔 취소됨", Toast.LENGTH_SHORT).show();
            } else {
                Log.e("scan", result.getContents());

                String scanCode = result.getContents();

                etAssetNo.setText(scanCode.trim());
                searchAssetNo(scanCode.trim());
            }

        } else {
            Log.d("MainActivity", "else");
            super.onActivityResult(requestCode, resultCode, data);
            switch (requestCode) {
                case CAMERA:
                    if (resultCode == Activity.RESULT_OK) {
                        try {

                            Log.e("mFileName", mFileName);
                            Log.e("mPath", mPath.getPath());

                            File file = new File(mPath, mFileName);

                            FileInputStream input_stream = new FileInputStream(file);

                            input_stream.close();

                            String currentPhotoPath = file.getAbsolutePath();

                            int degree = ImageUtil.GetExifOrientation(currentPhotoPath);

                            Bitmap rotateBitmap = ImageUtil.getRotatedBitmap(this, file.getAbsolutePath(), degree);
                            FileOutputStream outputStream = new FileOutputStream(mPath + "/" + mFileName);
                            rotateBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                            outputStream.close();

                            DisplayMetrics metrics = new DisplayMetrics();
                            WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
                            windowManager.getDefaultDisplay().getMetrics(metrics);

                            img.setImageBitmap(rotateBitmap);

                            System.gc();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }

        }
    }

    /**
     * 재물번호로 재물내용을 조회한다.
     * @param assetNo
     */
    private void searchAssetNo(String assetNo) {
        String url = "/ws/am/marketAsset/selectAsset";

        try {
            JSONObject param = new JSONObject();

            param.put("UNIVNO"  , OnbridPreference.getString(this, "univNo"));
            param.put("CAMPUSNO", OnbridPreference.getString(this, "campusNo"));
            param.put("DEPTNO"  , OnbridPreference.getString(this, "deptNo"));
            param.put("ASSETNO" , assetNo);
            param.put("PAGENUM" , 1);
            param.put("PAGEROW" , 1);

            OnbridRestClient.post(this, url, param, new OnbridResponseHandler(this) {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    //super.onSuccess(statusCode, headers, response);
                    Log.d("onSuccess", response.toString());
                    // {"USESTATE":0,"PRICE":2807000,"USEKINDNAME":null,"ASSETSTATENAME2":null,"ASSETTYPENAME":null, "CAMPUSNO":"1","GUBUNNAME":"냉난방기","DEPTNAME":"총장실",
                    // "ASSETNO":"310700400000145","SPEC":"2.5마력.4way. 삼성","DEPTNO":"11005","ASSETSTATE":255,"BARCODE":"310700400000145","QTY":1,"ASSETNAME":"냉난방기","NOMALGUBUN":0,
                    // "PLACENAME":"학원장비서실","NOMALGUBUNNAME":null,"GUBUNNO":"10700400","MODELNAME":"천장형 냉난방기(실외기+실내기)","UNIVNO":"0001","CAMPUSNAME":"서울캠퍼스",
                    // "AMOUNT":2807000,"ASSETSTATENAME":"정상","WRITEDATE":"20081117","ACCGUBUNNAME":null,"ACCGUBUN":"1","ROWNUM":1,"ASSETTYPE":"3","OLDASSETNO":"310700400000145",
                    // "USEKIND":null,"PLACENO":"A01204","DUDATE":null}
                    try {
                        if (response.getJSONArray("list").length() > 0) {
                            JSONObject jsonObject = (JSONObject) response.getJSONArray("list").get(0);

                            deptNo = jsonObject.getString("DEPTNO");
                            placeNo = jsonObject.getString("PLACENO");

                            tvAssetName.setText(jsonObject.getString("ASSETNAME"));
                            tvModelName.setText(jsonObject.getString("MODELNAME"));
                            tvSpec.setText(jsonObject.getString("SPEC"));
                            tvDeptName.setText(jsonObject.getString("DEPTNAME"));
                            tvPlaceName.setText(jsonObject.getString("PLACENAME"));
                            tvRecordDate.setText(jsonObject.getString("RECORDDATE"));

                            etAssetNo.setEnabled(false);
                            etTelNo.setEnabled(true);
                            btnCamera.setEnabled(true);

                            if (!OnbridPreference.getString(MarketPlusActivity.this, "deptNo").equalsIgnoreCase(jsonObject.getString("DEPTNO"))) {


                                AlertDialog.Builder builder = new AlertDialog.Builder(MarketPlusActivity.this);
                                builder.setMessage("부서가 틀린 재물은 신청할 수 없습니다.\r\n재물의 부서는 \"" + jsonObject.getString("DEPTNAME") + "\" 입니다");
                                builder.setTitle("재물 부서 오류");
                                builder.setPositiveButton("닫기", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        clearControll();
                                    }
                                });
                                builder.show();
                            }
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(MarketPlusActivity.this);
                            builder.setMessage("해당 재물은 없거나 이미 등록되어 있습니다.");
                            builder.setTitle("재물조회 오류");
                            builder.setPositiveButton("닫기", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    clearControll();
                                }
                            });
                            builder.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MarketPlusActivity.this);
                    builder.setMessage("해당 재물은 없거나 이미 등록되어 있습니다.");
                    builder.setTitle("재물조회 오류");
                    builder.setPositiveButton("닫기", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            clearControll();
                        }
                    });
                    builder.show();

                    super.onFailure(statusCode, headers, responseString, throwable);
                }
            });


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void openCamera(File path, String fileName) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = new File(mPath, mFileName);

        Uri uri = Uri.fromFile(file);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

        startActivityForResult(intent, CAMERA);
    }


    private void insertMarketAsset() {
        String url = "/ws/am/marketAsset/insertMarketAsset";
        JSONObject param = new JSONObject();

        try {
            param.put("UNIVNO"      , OnbridPreference.getString(this, "univNo"));
            param.put("CAMPUSNO"    , OnbridPreference.getString(this, "campusNo"));
            param.put("ASSETNO"     , etAssetNo.getText().toString().trim());
            param.put("REQDEPTNO"   , OnbridPreference.getString(this, "deptNo"));
            param.put("REQEMPNO"    , OnbridPreference.getString(this, "loginEmp"));
            param.put("RETURNGUBN"  , 1);
            param.put("FROMDEPTNO"  , deptNo);
            param.put("FROMPLACENO" , placeNo);
            param.put("FROMEMPNO"   , "");
            param.put("FROMUSEEMPNAME", "");
            param.put("MNGTEL"      , etTelNo.getText().toString().trim());
            param.put("MNGEMAIL"    , "");
            param.put("LOGINEMP"    , OnbridPreference.getString(this, "loginEmp"));


            OnbridRestClient.post(this, url, param, new OnbridResponseHandler(this) {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.d("onSuccess", response.toString());
                    insertFile();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void insertFile() {
        String id = String.valueOf(new Date().getTime());
        String url = OnbridPreference.getString(this, "url") + "/insertFileList";

        Map param = new HashMap();
        List list = new ArrayList();
        File file = new File(mPath, mFileName);

        Map fileMap = new HashMap();
        fileMap.put("UNIVNO"       , OnbridPreference.getString(this, "univNo"));
        fileMap.put("CAMPUSNO"     , OnbridPreference.getString(this, "campusNo"));
        fileMap.put("CATEGORYITEM" , etAssetNo.getText().toString().trim());
        fileMap.put("ID"           , id);
        fileMap.put("CATEGORY"     , "MARKET_ASSET");
        fileMap.put("NAME"         , tvModelName.getText().toString().trim());
        fileMap.put("NAMER"        , file.getName());
        fileMap.put("EXT"          , JPEG_FILE_SUFFIX);
        fileMap.put("FILESIZE"     , (file.length()/1024));
        fileMap.put("PATH"         , "");
        fileMap.put("TYPE"         , "image/jpeg");
        fileMap.put("BIGO", "");
        fileMap.put("LOGINEMP", OnbridPreference.getString(this, "loginEmp"));

        list.add(fileMap);
        try {
            param.put("list", list);

            Log.e("file", url);
            Log.e("file", param.toString());


            String _message = new Gson().toJson(param);
            ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

            AsyncHttpClient client = new AsyncHttpClient();
            client.post(this, url, entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    //super.onSuccess(statusCode, headers, response);
                    Log.e("FileList.onSuccess", response.toString());

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    //super.onFailure(statusCode, headers, responseString, throwable);
                    //Toast.makeText(getApplicationContext(), "정보가 없습니다.", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            uploadImageFile();
        }


    }


    private void uploadImageFile() {

        String url = "/uploadFile";
        File file = new File(mPath, mFileName);

        try {
            RequestParams params = new RequestParams();
            params.put("UNIVNO"    , OnbridPreference.getString(this, "univNo"));
            params.put("CAMPUSNO"  , OnbridPreference.getString(this, "campusNo"));
            params.put("CAMPUSNO"  , OnbridPreference.getString(this, "campusNo"));
            params.put("LOGINEMP"  , OnbridPreference.getString(this, "loginEmp"));
            params.put("FILE"      , file);
            params.put("CATEGORY"  , "MARKET_ASSET" + file.getName().split("_")[1]);

            OnbridRestClient.put(this, url, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.e("onSuccess", response.toString());
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.e("onFailure", errorResponse.toString());
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("등록되었습니다.");
            builder.setTitle("등록성공");
            builder.setPositiveButton("닫기", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    clearControll();
                }
            });
            builder.show();

        }

    }




}
