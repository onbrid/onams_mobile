package com.onbrid.onams.onams_mobile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class ImageRotateActivity extends AppCompatActivity {

    private ImageView rotateImg;

    private String filePath;
    private String fileName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_rotate);

        rotateImg = (ImageView) findViewById(R.id.rotateImg);

        findViewById(R.id.btnRotate).setOnClickListener(btnListener);
        findViewById(R.id.btnInit)  .setOnClickListener(btnListener);
        findViewById(R.id.btnSave)  .setOnClickListener(btnListener);

        filePath = getIntent().getExtras().getString("FILEPATH");
        fileName = getIntent().getExtras().getString("FILENAME");

    }


    private View.OnClickListener btnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.btnRotate:

                    break;
                case R.id.btnInit:

                    break;
                case R.id.btnSave:

                    break;
            }

        }
    };



}
