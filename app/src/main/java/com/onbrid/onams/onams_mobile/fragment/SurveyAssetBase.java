package com.onbrid.onams.onams_mobile.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.onbrid.onams.onams_mobile.R;
import com.onbrid.onams.onams_mobile.SearchCode;
import com.onbrid.onams.onams_mobile.SurveyNomalActivity;
import com.onbrid.onams.onams_mobile.adaptor.AssetSearchOptionAdaptor;
import com.onbrid.onams.onams_mobile.model.Code;
import com.onbrid.onams.onams_mobile.model.Option;
import com.onbrid.onams.onams_mobile.util.ImageUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class SurveyAssetBase extends Fragment {

    private View rootView;
    private ListView listViewOption;
    private ImageView iv;

    private AssetSearchOptionAdaptor assetSearchOptionAdaptor;

    private Map searchOption;
    private String mFileName;
    private String mCurrentPhotoPath;
    private int    mDegree = 0;

    // 리스트뷰 롱클릭 이벤트 포지션
    private int selectedPos = -1;

    private File path;

    private boolean workFlag = false;

    private final static String OPTION_PLAN     = "PLAN";
    private final static String OPTION_PLACE    = "PLACE";
    private final static String OPTION_DEPT     = "DEPT";
    private final static String OPTION_SCAN_YN  = "SCAN_YN";

    private static final String JPEG_FILE_PATH   = "ONBRID/PLACE";
    private static final String JPEG_FILE_PREFIX = "PL_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // return super.onCreateView(inflater, container, savedInstanceState);

        rootView = inflater.inflate(R.layout.fragment_survey_asset_base, container, false);

        /* enable onCreateOptionMenu */
        setHasOptionsMenu(true);

        this.initListViewOption();

        iv = (ImageView) rootView.findViewById(R.id.placeImg);
        rootView.findViewById(R.id.btnNomal).setOnClickListener(btnListenor);
        // rootView.findViewById(R.id.btnAll).setOnClickListener(btnListenor);

        path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/" + JPEG_FILE_PATH + "/" + new SimpleDateFormat("yyyyMMdd").format(new Date()));
        if (!path.exists()) path.mkdirs();

        /* 카메라 연결 */
        rootView.findViewById(R.id.btnCamera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* 장소 입력 검사. */
                if ( !searchOption.containsKey("PLACENO") || searchOption.get("PLACENO").toString().equalsIgnoreCase("") ) {
                    Toast.makeText(getContext(), "장소를 지정해 주세요.", Toast.LENGTH_LONG).show();
                    return;
                }

                /* 기존사진 삭제 */
                if (mFileName != null && mFileName.split("_")[0] == searchOption.get("PLACENO").toString()) {
                    deletePlacePic();
                }

                String phoneNum = ((TelephonyManager) getContext().getSystemService(getContext().TELEPHONY_SERVICE)).getLine1Number();

                mFileName = searchOption.get("PLACENAME").toString() + "_" + searchOption.get("PLACENO").toString() + "_" +
                            JPEG_FILE_PREFIX + phoneNum + "_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + JPEG_FILE_SUFFIX;

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                File file = new File(path, mFileName);

                // if ( !path.exists() ) path.mkdirs();

                Uri uri = Uri.fromFile(file);

                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

                startActivityForResult(intent, 0);
            }
        });

        return rootView;
    }






    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0 : // 카메라 촬영
                if (resultCode == Activity.RESULT_OK) {
                    try {

                        File file = new File(path, mFileName);

                        FileInputStream input_stream = new FileInputStream(file);

                        input_stream.close();

                        mCurrentPhotoPath = file.getAbsolutePath();


                        mDegree = ImageUtil.GetExifOrientation(mCurrentPhotoPath);

                        Bitmap rotateBitmap = ImageUtil.getRotatedBitmap(getContext(), file.getAbsolutePath(), mDegree);
                        FileOutputStream outputStream = new FileOutputStream(path + "/" + mFileName);
                        rotateBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                        outputStream.close();

                        DisplayMetrics metrics = new DisplayMetrics();
                        WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
                        windowManager.getDefaultDisplay().getMetrics(metrics);

                        //ImageView iv = (ImageView) rootView.findViewById(R.id.placeImg);
                        iv.setImageBitmap(rotateBitmap);

//                        ImageView iv = new ImageView(getContext());
//                        iv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//
//                        ViewGroup.LayoutParams params = iv.getLayoutParams();
//                        params.width = 400;
//                        params.height = 400;
//                        iv.setLayoutParams(params);
//
//                        iv.setImageBitmap(roundBitmap);
//                        //((LinearLayout)findViewById(R.id.imgLayout)).addView(iv);
//                        ((GridLayout)rootView.findViewById(R.id.imgLayout)).addView(iv);


                        //imageView.setImageBitmap(roundBitmap);

                        //input_stream.close();

                        System.gc();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case 1 : // 공통코드 조회

                if (resultCode == Activity.RESULT_OK) {
                    Code returnCode = data.hasExtra("code") ? (Code) data.getSerializableExtra("code") : null;

                    if (mFileName != null && returnCode.getNo() != mFileName.split("_")[0]) {
                        iv.setImageDrawable(null);
                        workFlag = false;
                    }

                    String type     = data.getStringExtra("type");
                    int position    = data.getIntExtra("position", -1);

                    if (returnCode == null || position == -1) {
                        return;
                    }

                    ((Option) assetSearchOptionAdaptor.getItem(position)).set(returnCode);

                    switch (type) {
                        case OPTION_PLACE   :
                            searchOption.put("PLACENO"  , returnCode.getNo());
                            searchOption.put("PLACENAME", returnCode.getName());
                            break;
                        case OPTION_DEPT    :
                            searchOption.put("DEPTNO"   , returnCode.getNo());
                            searchOption.put("DEPTNAME" , returnCode.getName());
                            break;
                    }

                    assetSearchOptionAdaptor.notifyDataSetChanged();
                }

                break;
        }



    }


    @Override
    public void onStop() {
        super.onStop();
        if ( workFlag == false && mFileName != null) {
            // 작업을 안했으므로 장소 사진을 지운다.
            deletePlacePic();
        }
    }


    /**
     * 버튼 이벤트 연결
     */
    View.OnClickListener btnListenor = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnNomal :

                    /* 필수값 체크 */
                    if (searchOption.get("PLACENO").toString().equalsIgnoreCase("")) {
                        Toast.makeText(getContext(), "장소를 지정해 주세요.", Toast.LENGTH_LONG).show();
                        return;
                    }

                    workFlag = true;
                    //Log.e("btnNomal", searchOption.toString());
                    Intent intent = new Intent(getActivity(), SurveyNomalActivity.class);
                    intent.putExtra("SCANPLACENO"  , searchOption.get("PLACENO").toString());
                    intent.putExtra("SCANPLACENAME", searchOption.get("PLACENAME").toString());
                    intent.putExtra("SCANDEPTNO"   , searchOption.get("DEPTNO").toString());
                    intent.putExtra("SCANDEPTNAME" , searchOption.get("DEPTNAME").toString());

                    startActivity(intent);
                    break;
//                case R.id.btnAll :
//                    Toast.makeText(getContext(), "btnAll", Toast.LENGTH_SHORT).show();
//                    break;
            }
        }
    };


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        //inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    private void initParam() {
        if (searchOption == null)
            searchOption = new HashMap();
        searchOption.put("PAGENUM", 1);
        searchOption.put("PAGEROW", 20);

        searchOption.put("PLACENO", "");
        searchOption.put("PLACENAME", "");
        searchOption.put("DEPTNO", "");
        searchOption.put("DEPTNAME", "");
        searchOption.put("PLANNO", "");
        searchOption.put("PLANSTATUS", "");
    }

    private void initListViewOption() {

        this.initParam();

        /* 조회조건 추가 */
        ArrayList<Option> options = new ArrayList<>();
        options.add(new Option("0001", "위치정보", OPTION_PLACE, R.drawable.ic_action_place));
        options.add(new Option("0002", "부서정보", OPTION_DEPT , R.drawable.ic_action_group));

        options.get(0).setRequired(true);

        /* 검색조건 리스트뷰/아답터 초기화 */
        listViewOption = (ListView) rootView.findViewById(R.id.listviewOption);
        assetSearchOptionAdaptor = new AssetSearchOptionAdaptor(getActivity(), options);
        listViewOption.setAdapter(assetSearchOptionAdaptor);

        /* 검색조건 리스트 클릭 이벤트 */
        listViewOption.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Option option = (Option) listViewOption.getItemAtPosition(position);

                Intent intent = new Intent(getActivity(), SearchCode.class);
                intent.putExtra("position", position);
                intent.putExtra("type", option.getType());
                startActivityForResult(intent, 1);

            }
        });

        /* 검색조건 리스트 롱!! 클릭 이벤트 ## 검색 조건을 초기화한다. */
        listViewOption.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                String title = "";

                selectedPos = position;

                switch (selectedPos) {
                    case 0 : title = "장소"; break;
                    case 1 : title = "부서"; break;
                }

                AlertDialog.Builder alertDlg = new AlertDialog.Builder(view.getContext());
                alertDlg.setTitle(title + "조건 초기화");

                alertDlg.setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ((Option) assetSearchOptionAdaptor.getItem(selectedPos)).clear();
                        assetSearchOptionAdaptor.notifyDataSetChanged();

                        switch (selectedPos) {
                            case 0 :
                                searchOption.put("PLACENO"  , "");
                                searchOption.put("PLACENAME", "");
                                break;
                            case 1 :
                                searchOption.put("DEPTNO", "");
                                searchOption.put("DEPTNAME", "");
                                break;
                        }

                        dialog.dismiss();
                    }
                });

                alertDlg.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDlg.show();


                return false;
            }
        });

    }

    /**
     * 장소 사진파일 삭제
     */
    private void deletePlacePic() {
        File deleteFile = new File(path, mFileName);
        if ( deleteFile.exists() ) {
            deleteFile.delete();
        }
    }


}
