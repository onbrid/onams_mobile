package com.onbrid.onams.onams_mobile;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.onbrid.onams.onams_mobile.adaptor.CodeListAdaptor;
import com.onbrid.onams.onams_mobile.dialog.OnbridProgressDialog;
import com.onbrid.onams.onams_mobile.model.Code;
import com.onbrid.onams.onams_mobile.util.OnbridPreference;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;

public class SearchCode extends ActionBarActivity implements SearchView.OnQueryTextListener {

    private Toolbar toolbar;

    CodeListAdaptor codeListAdaptor;
    private ListView listView;
    private TextView description;
    private SearchView searchView;

    private OnbridProgressDialog progressDialog;

    private Map<String, Object> searchParams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_code);

        description = (TextView) findViewById(R.id.description);
        listView    = (ListView) findViewById(R.id.listView);

        /* 툴바 정의 */
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        /* 기본 리턴값 정의 */
        setResult(-1, getIntent());

        /* 리스트 초기화 */
        codeListAdaptor = new CodeListAdaptor(this, new ArrayList());
        listView.setAdapter(codeListAdaptor);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Code code = (Code) parent.getAdapter().getItem(position);

                Log.e("code", code.getName());

                Intent intent = getIntent();
                intent.putExtra("position", getIntent().getIntExtra("position", -1));
                intent.putExtra("type",     getIntent().getStringExtra("type"));
                intent.putExtra("code",     code);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        /* 기초 데이터 생성 */
        searchParams = new HashMap<String, Object>();
        searchParams.put("LOGINEMP", "1001");
        //searchParams.put("URL", "/selectCommCode");

        String type = getIntent().getStringExtra("type");

        searchParams.put("RETURNCODE", type);

        if (type.equalsIgnoreCase("PLAN")) {

            searchParams.put("TYPE", "PLAN");
            searchParams.put("TYPEKEY", "PLANNO");
            searchParams.put("TYPEVALUE", "PLANNAME");

        } else if (type.equalsIgnoreCase("PLACE")) {

            getSupportActionBar().setTitle("위치조회");

            searchParams.put("TYPE", "PLACE");
            searchParams.put("TYPEKEY", "PLACENAME");
            searchParams.put("TYPEVALUE", "PLACENO");

        } else if (type.equalsIgnoreCase("DEPT")) {

            getSupportActionBar().setTitle("부서조회");

            searchParams.put("TYPE", "DEPT");
            searchParams.put("TYPEKEY", "DEPTNAME");
            searchParams.put("TYPEVALUE", "DEPTNO");

        }

    }

    /*@Override
    protected void onStart() {
        super.onStart();
    }*/

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /* Option menu 추가 */
        getMenuInflater().inflate(R.menu.search_code, menu);

        /* EditText가 있는 View가져오기 */
        MenuItem menuItem = menu.findItem(R.id.search);

        /* SearchView 생성 */
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint("검색어를 입력하세요.");
        if (searchView != null) {
            searchView.setOnQueryTextListener(this);
        }

        //searchView.onActionViewCollapsed();
        searchView.setQueryRefinementEnabled(true);
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("searchView", "onClick");
            }
        });
        searchView.onActionViewExpanded();

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.e("SearchCode", "onQueryTextSubmit : " + query);
        codeListAdaptor.clear();
        searchView.clearFocus();
        // new DataManager().execute( searchParams, query );

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null &&  networkInfo.isConnected()) {

            try {

                String url = OnbridPreference.getString(SearchCode.this, "url") + "/ws/selectCommCode";
                searchParams.put("UNIVNO", OnbridPreference.getString(SearchCode.this, "univNo"));
                searchParams.put("CAMPUSNO", OnbridPreference.getString(SearchCode.this, "campusNo"));
                searchParams.put("LOGINEMP", OnbridPreference.getString(SearchCode.this, "userId"));
                if (getIntent().getStringExtra("type").equalsIgnoreCase("PLACE")) {
                    searchParams.put("PLACENAME", query);
                } else if (getIntent().getStringExtra("type").equalsIgnoreCase("DEPT")) {
                    searchParams.put("DEPTNAME", query);
                }

                String _message = new Gson().toJson(searchParams);
                System.out.println(_message);
                System.out.println("----------------------------------------------------------------");

                ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

                final ArrayList<Code> data = new ArrayList<Code>();

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(getParent(), url, entity, "application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        // super.onSuccess(statusCode, headers, response);
                        try {
                            // {"LIST":[{"ROOMNAME":null,"HIGHPLACE":"0002","CAMPUSNO":"01","CAMPUSNAME":"OnBrid-대학","ROWNO":2,"FLOOR":null,"USECODE":null,"PLACENO":"0007","PLACELEVEL":1,"PLACENAME":"개발 연구실"}]
                            // ,"MESSAGE":"Success","RESULT":0}
                            Log.e("SearchCode", response.toString());
                            JSONArray jsonArray = response.getJSONArray("LIST");

                            JSONObject jsonObject;
                            for (int i=0; i<jsonArray.length(); i++) {
                                jsonObject = (JSONObject) jsonArray.get(i);
                                data.add(new Code( jsonObject.getString(searchParams.get("TYPEVALUE").toString()), jsonObject.getString(searchParams.get("TYPEKEY").toString()) ));
                            }

                            codeListAdaptor.addAll( data );

                            if( codeListAdaptor.getCount() == 0  ){

                                description.setVisibility(View.VISIBLE);

                                Toast toast = Toast.makeText(getApplicationContext(), "조건에 해당하는 항목이 없습니다.", Toast.LENGTH_SHORT);
                                toast.show();
                            }else{
                                description.setVisibility(View.INVISIBLE);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(getApplicationContext(), "네트워크 연결상태를 확인해 주세요.", Toast.LENGTH_SHORT).show();
        }



        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.i("SearchCode", "onQueryTextChange : " + newText);
        if (newText.length() == 0) {
            codeListAdaptor.clear();
            description.setVisibility(View.VISIBLE);
        }

        return false;
    }

    public void showProgress(boolean show) {

        if (show == true) {
            try {
                progressDialog = new OnbridProgressDialog(this);
                progressDialog.show();
            } catch (Exception e) {}
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

    }

}
