package com.onbrid.onams.onams_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.onbrid.onams.onams_mobile.util.OnbridPreference;
import com.onbrid.onams.onams_mobile.util.OnbridResponseHandler;
import com.onbrid.onams.onams_mobile.util.OnbridRestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

import cz.msebera.android.httpclient.Header;

public class AssetViewActivity extends AppCompatActivity {

    private Toolbar toolbar;

    private ImageView assetImage;
    private TextView  assetNo;
    private TextView  assetName;
    private TextView  assetPlace;
    private TextView  assetDept;
    private TextView  assetModel;
    private TextView  assetSpec;
    private TextView  assetDate;
    private TextView  assetAmount;
    private TextView  assetState;
    private TextView  assetGubn;
    private TextView  assetBigo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_view);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        setControll();
        getAssetDetail();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_asset_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        finish();

        return super.onOptionsItemSelected(item);
    }


    /**
     * EditText, TextView 에 text(내용)을 셋한다.
     * @param item
     * @param val
     */
    private void setText(TextView item, Object val) {
        item.setText(val.toString().equalsIgnoreCase("null") ? "-" : val.toString());
    }

    private void setText(TextView item, Object val, String type) {
        String value = "";

        if (type.equalsIgnoreCase("date")) {
            value = val.toString().substring(0,4) + "-" + val.toString().substring(4,6) + "-" + val.toString().substring(6,8);
        } else if (type.equalsIgnoreCase("decimal")) {
            DecimalFormat format = new DecimalFormat("#,###");
            value = (String) format.format(Integer.parseInt(val.toString()));
        } else {
            value = (String) val;
        }

        setText(item, value);
    }

    private void setControll() {
        assetImage   = (ImageView) findViewById(R.id.assetImage);
        assetNo      = (TextView) findViewById(R.id.assetNo    );
        assetName    = (TextView) findViewById(R.id.assetName  );
        assetPlace   = (TextView) findViewById(R.id.assetPlace );
        assetDept    = (TextView) findViewById(R.id.assetDept  );
        assetModel   = (TextView) findViewById(R.id.assetModel );
        assetSpec    = (TextView) findViewById(R.id.assetSpec  );
        assetDate    = (TextView) findViewById(R.id.assetDate  );
        assetAmount  = (TextView) findViewById(R.id.assetAmount);
        assetState   = (TextView) findViewById(R.id.assetState );
        assetGubn    = (TextView) findViewById(R.id.assetGubn  );
        assetBigo    = (TextView) findViewById(R.id.assetBigo  );
    }

    private void getAssetDetail() {

        try {
            String url = "/ws/pda/asset/select";
            Intent intent = getIntent();

            // {DATABLOCK=[{UNIVNO=0001, LOGINEMP=1001, PLANNO=20150025, PDANO=101, CAMPUSNO=01, ASSETNO=201500000019}]}
            JSONObject param = new JSONObject();
            JSONArray  dataBlock = new JSONArray();
            JSONObject paramInner = new JSONObject();

            paramInner.put("UNIVNO"  , OnbridPreference.getString(this, "univNo"));
            paramInner.put("CAMPUSNO", OnbridPreference.getString(this, "campusNo"));
            paramInner.put("ASSETNO" , intent.getStringExtra("ASSETNO"));
            dataBlock.put(paramInner);
            param.put("DATABLOCK", dataBlock);
            // Log.d("param", param.toString());

            OnbridRestClient.post(this, url, param, new OnbridResponseHandler(this) {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.d("onSuccess", response.toString());
                    // {"USESTATE":0,"PRICE":42000,"USEKINDNAME":"공기구비품","ASSETSTATENAME2":null,"BIGO":null,"ASSETTYPENAME":"공기구비품","CAMPUSNO":"01","GUBUNNAME":"책상",
                    // "DEPTNAME":"기획 관리부","ASSETNO":"201500000003","SPEC":"1200*700*720","DEPTNO":"0001","ASSETSTATE":255,"BARCODE":"2015000003","QTY":1,"ASSETNAME":"책상",
                    // "NOMALGUBUN":0,"PLACENAME":"기획 관리부 사무실","NOMALGUBUNNAME":null,"GUBUNNO":"0001","MODELNAME":"703 탑책상","UNIVNO":"0001","CAMPUSNAME":"OnBrid-대학",
                    // "AMOUNT":42000,"ASSETSTATENAME":"정상","WRITEDATE":"20150907","ACCGUBUNNAME":null,"ACCGUBUN":null,"ROWNUM":1,"ASSETTYPE":"0008","OLDASSETNO":null,"USEKIND":"0016","PLACENO":"0003","DUDATE":null}
                    try {
                        setText(assetNo, response.getString("ASSETNO"));
                        setText(assetName, response.getString("ASSETNAME"));
                        setText(assetPlace, response.getString("PLACENAME"));
                        setText(assetDept, response.getString("DEPTNAME"));
                        setText(assetModel, response.getString("MODELNAME"));
                        setText(assetSpec, response.getString("SPEC"));
                        setText(assetDate, response.getString("WRITEDATE"), "date");
                        setText(assetAmount, response.getString("AMOUNT"), "decimal");
                        setText(assetState, response.getString("ASSETSTATENAME"));
                        setText(assetGubn, response.getString("ASSETTYPENAME"));
                        setText(assetBigo, response.getString("BIGO"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }




} // class
