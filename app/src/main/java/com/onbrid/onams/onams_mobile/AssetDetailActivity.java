package com.onbrid.onams.onams_mobile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.onbrid.onams.onams_mobile.model.AssetScanItem;
import com.onbrid.onams.onams_mobile.util.OnbridPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;

public class AssetDetailActivity extends ActionBarActivity {

    private Toolbar toolbar;

    private AssetScanItem extraItem;

    // private ArrayList<Map> fileList = new ArrayList<Map>();
    private JSONArray fileList ;

    private int surveyState;

    private LinearLayout btnClear;
    private LinearLayout btnYes;
    private LinearLayout btnNo;

    private ViewFlipper flipper;

    private ImageView img1;
    private ImageView ivClear;
    private ImageView ivYes;
    private ImageView ivNo;

    private TextView tvClear;
    private TextView tvYes;
    private TextView tvNo;

    private int DEFAULT_COLOR ;
    private int SUCCESS_COLOR ;

    /**
     * 초기화, 있음, 없음 버튼에 대한 처리이다.
     */
    View.OnClickListener btnSurveyListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnClear:
                    surveyState = -1;
                    updateSurvetAsset(-1, -1, "");
                    break;
                case R.id.btnYes:
                    surveyState = 0;
                    updateSurvetAsset(0, 0, "");
                    break;
                case R.id.btnNo:
                    surveyState = 1;
                    showCustomDialog(1);
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_detail);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        DEFAULT_COLOR = getResources().getColor(R.color.SecondaryText);
        SUCCESS_COLOR = Color.parseColor("#5cb85c");

        surveyState = -1;

        extraItem = (AssetScanItem) getIntent().getSerializableExtra("asset");

//        flipper= (ViewFlipper)findViewById(R.id.flipper);
        img1   = (ImageView) findViewById(R.id.img01);

        ivClear = (ImageView) findViewById(R.id.ivClear);
        ivYes   = (ImageView) findViewById(R.id.ivYes);
        ivNo    = (ImageView) findViewById(R.id.ivNo);
        tvClear = (TextView)  findViewById(R.id.tvClear);
        tvYes   = (TextView)  findViewById(R.id.tvYes);
        tvNo    = (TextView)  findViewById(R.id.tvNo);

        ((TextView) findViewById(R.id.assetName)).setText(extraItem.getName());
        ((TextView) findViewById(R.id.assetId)).setText(extraItem.getId());
        ((TextView) findViewById(R.id.assetPlace)).setText(extraItem.getPlaceName());
        ((TextView) findViewById(R.id.assetDept)).setText(extraItem.getDeptName());

        btnClear = (LinearLayout) findViewById(R.id.btnClear);
        btnYes   = (LinearLayout) findViewById(R.id.btnYes);
        btnNo    = (LinearLayout) findViewById(R.id.btnNo);

        btnClear.setOnClickListener(btnSurveyListener);
        btnYes.setOnClickListener(btnSurveyListener);
        btnNo.setOnClickListener(btnSurveyListener);

        this.setCheckedState(extraItem.getStatus());
        this.getImages();
//        ((ImageView) findViewById(R.id.ivClear)).setColorFilter(SUCCESS_COLOR);
//        ((TextView) findViewById(R.id.tvClear)).setTextColor(SUCCESS_COLOR);

    }




    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = getIntent();
        intent.putExtra("position", getIntent().getSerializableExtra("position"));
        intent.putExtra("surveyState", surveyState);
        setResult(RESULT_OK, intent);
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        intent.putExtra("position", getIntent().getSerializableExtra("position"));
        intent.putExtra("surveyState", surveyState);
        setResult(RESULT_OK, intent);
        finish();

        super.onBackPressed();
    }





    /**
     * 없음 클릭시 커스텀 다이얼로그를 띄운다.
     * @param isExsist
     */
    private void showCustomDialog(int isExsist) {
        final int isExs = isExsist;

        //Dialog에서 보여줄 입력화면 View 객체 생성 작업
        //Layout xml 리소스 파일을 View 객체로 부불려 주는(inflate) LayoutInflater 객체 생성
        LayoutInflater inflater=getLayoutInflater();

        //res폴더>>layout폴더>>dialog_addmember.xml 레이아웃 리소스 파일로 View 객체 생성
        //Dialog의 listener에서 사용하기 위해 final로 참조변수 선언
        final View dialogView= inflater.inflate(R.layout.dialog_deptsurvey_no, null);

        //멤버의 세부내역 입력 Dialog 생성 및 보이기
        AlertDialog.Builder buider= new AlertDialog.Builder(this); //AlertDialog.Builder 객체 생성
        buider.setTitle("사유 설정"); //Dialog 제목
        buider.setIcon(android.R.drawable.ic_menu_add); //제목옆의 아이콘 이미지(원하는 이미지 설정)
        buider.setView(dialogView); //위에서 inflater가 만든 dialogView 객체 세팅 (Customize)
        buider.setPositiveButton("저장", new DialogInterface.OnClickListener() {
            //Dialog에 "Complite"라는 타이틀의 버튼을 설정
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

                //dialogView 객체 안에서 NAME을 입력받는 EditText 객체 찾아오기(주의: dialaogView에서 find 해야함)
                EditText edit_Bigo= (EditText)dialogView.findViewById(R.id.dialog_edit);

                //dialogView 객체 안에서 NATION을 입력받는 RadioGroup 객체 찾아오기
                RadioGroup rg= (RadioGroup)dialogView.findViewById(R.id.dialog_rg);

                if ( rg.getCheckedRadioButtonId() < 0 ) {
                    Toast.makeText(AssetDetailActivity.this, "사유를 선택하세요.", Toast.LENGTH_SHORT).show();
                } else {
                    //선택된 RadioButton의 ID를 RadioGroup에게 얻어오기
                    int checkedId= rg.getCheckedRadioButtonId();

                    //Check 된 RadioButton의 ID로 라디오버튼 객체 찾아오기
                    RadioButton rb= (RadioButton)rg.findViewById(checkedId);
                    String nation= rb.getText().toString();//RadionButton의 Text 얻어오기
                    // 11:반출, 12:망실, 13:수리, 14:tag파손, 19:기타
                    int assetState = -1;
                    switch (nation) {
                        case "반출"    : assetState = 11; break;
                        case "망실"    : assetState = 12; break;
                        case "수리"    : assetState = 13; break;
                        case "tag파손" : assetState = 14; break;
                        case "기타"    : assetState = 19; break;
                    }
                    //Toast.makeText(AssetDetailActivity.this, checkedId+ "/"+nation+"/"+bigo, Toast.LENGTH_LONG).show();

                    updateSurvetAsset(isExs, assetState, edit_Bigo.getText().toString());
                }

            }
        });
        buider.setNegativeButton("취소", new DialogInterface.OnClickListener() {
            //Dialog에 "Cancel"이라는 타이틀의 버튼을 설정

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                //멤버 정보의 입력을 취소하고 Dialog를 종료하는 작업
                //취소하였기에 특별한 작업은 없고 '취소'했다는 메세지만 Toast로 출력
                Toast.makeText(AssetDetailActivity.this, "작업을 취소합니다", Toast.LENGTH_SHORT).show();
            }
        });

        //설정한 값으로 AlertDialog 객체 생성
        AlertDialog dialog=buider.create();

        //Dialog의 바깥쪽을 터치했을 때 Dialog를 없앨지 설정
        dialog.setCanceledOnTouchOutside(false);//없어지지 않도록 설정

        //Dialog 보이기
        dialog.show();

    }


    /**
     * 부서재물조사 있음/없음 통신을한다.
     * @param isExsist
     * @param assetState
     * @param bigo
     */
    private void updateSurvetAsset( int isExsist, int assetState, String bigo ) {
        final int isExs = isExsist;

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            try {
                String url = OnbridPreference.getString(AssetDetailActivity.this, "url") + "/ws/sa/surveyDept/updateReadGubn";

                Map param = new HashMap();
                param.put("UNIVNO"           , OnbridPreference.getString(AssetDetailActivity.this, "univNo"));
                param.put("CAMPUSNO"         , OnbridPreference.getString(AssetDetailActivity.this, "campusNo"));
                param.put("PLANNO"           , getIntent().getSerializableExtra("PLANNO"));
                param.put("ASSETNO"          , extraItem.getId());
                param.put("PLACENO"          , extraItem.getPlaceNo());
                param.put("DEPTNO"           , extraItem.getDeptNo());
                param.put("USEEMPNO"         , "");
                param.put("ACTIONGUBN"       , 1);
                param.put("READGUBN"         , 4);
                param.put("SURVEYGUBN"       , (getIntent().getSerializableExtra("SCANPLACENO").equals(""))? 1 : 0); // 장소기반 부서기반.
                param.put("DEPTSURVEYGUBN"   , (isExsist == -1)?   null : isExsist); // 있음.없음.미확ㄷ인
                param.put("SURVEYASSETSTATE" , (assetState == -1)? null : assetState); // 0정상 1추가등등 11:반출, 12:망실, 13:수리, 14:tag파손, 19:기타
                param.put("SCANPLACENO"      , (getIntent().getSerializableExtra("SCANPLACENO").equals(""))? extraItem.getPlaceNo() : getIntent().getSerializableExtra("SCANPLACENO"));
                param.put("SCANDEPTNO"       , getIntent().getSerializableExtra("SCANDEPTNO"));
                param.put("SCANEMPNO", OnbridPreference.getString(AssetDetailActivity.this, "loginEmp"));
                param.put("BIGO"             , bigo);
                param.put("LOGINEMP"         , OnbridPreference.getString(AssetDetailActivity.this, "loginEmp"));
                // Log.e("closeItem.param", param.toString());

                String _message = new Gson().toJson(param);

                ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(getParent(), url, entity, "application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        // super.onSuccess(statusCode, headers, response);
                        try {
                            Log.e("closeItem", response.toString());

                            extraItem.setStatus(isExs);
                            setCheckedState(isExs);
//                            assetListAdaptor.notifyDataSetChanged();
//                            listViewAsset.closeAnimate(posi);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(getApplicationContext(), "네트워크 연결상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
        }

    }

    private void setCheckedState(int status) {
        ivClear.setColorFilter(DEFAULT_COLOR);
        ivYes.setColorFilter(DEFAULT_COLOR);
        ivNo.setColorFilter(DEFAULT_COLOR);
        tvClear.setTextColor(DEFAULT_COLOR);
        tvYes.setTextColor(DEFAULT_COLOR);
        tvNo.setTextColor(DEFAULT_COLOR);

        switch (status) {
            case -1 :
                ivClear.setColorFilter(SUCCESS_COLOR);
                tvClear.setTextColor(SUCCESS_COLOR);
                break;
            case 0 :
                ivYes.setColorFilter(SUCCESS_COLOR);
                tvYes.setTextColor(SUCCESS_COLOR);
                break;
            case 1 :
                ivNo.setColorFilter(SUCCESS_COLOR);
                tvNo.setTextColor(SUCCESS_COLOR);
                break;
        }

    }


    private void getImages() {

        try {
            String url = OnbridPreference.getString(this, "url") + "/fileSearch";

            Map param = new HashMap();
            param.put("UNIVNO"           , OnbridPreference.getString(this, "univNo"));
            param.put("CAMPUSNO"         , OnbridPreference.getString(this, "campusNo"));
            param.put("CATEGORY"         , "images");
            param.put("CATEGORYITEM"     , extraItem.getId());
            param.put("LOGINEMP"         , OnbridPreference.getString(this, "loginEmp"));
            // Log.e("closeItem.param", param.toString());

            String _message = new Gson().toJson(param);

            ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

            AsyncHttpClient client = new AsyncHttpClient();
            client.post(getParent(), url, entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    // super.onSuccess(statusCode, headers, response);
                    try {
                        Log.e("getImageUrl", response.toString());
                        // [{"BIGO":null,"NAME":"Chrysanthemum.jpg","REGDATE":1449041839000,"CAMPUSNO":"01","FILESIZE":858,"UNIVNO":"0001",
                        // "PATH":"c:\\onbrid\\onams\\data\\files\\0001\\01\\images","CATEGORYITEM":"201500000019","REGEMP":"1001","EXT":".jpg",
                        // "NAMER":"201500000019_1449041837372_Chrysanthemum.jpg","CATEGORY":"images","ID":"1449041837372"
                        // ,"URL":"\/files\/0001\/01\/images\/201500000019_1449041837372_Chrysanthemum.jpg","TYPE":"image\/jpeg"}]
                        fileList = response;

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        setImages();
                    }

                }

                @Override
                public void onFinish() {
                    Log.e("getImageUrl", "onFinish");
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void setImages() {
        if (fileList == null) {
            Log.e("fileList", "nullnullnullnullnullnull");
            return;
        } else {
            Log.e("fileList", fileList.length()+"");
            Log.e("fileList", fileList.toString());
        }
        try {

            JSONObject item;
            for (int i=0; i<fileList.length(); i++) {

                item = (JSONObject) fileList.get(i);
                uri = OnbridPreference.getString(this, "url") + item.getString("URL");

                Log.e("fileList", item.getString("URL"));
            }


            new WebGetImage().execute();
            img1.setImageBitmap(bit);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private Bitmap bit;
    private String uri = "";
    // 네트워크에 접속하여 이미지를 가져오는 클래스 선언
    class WebGetImage extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // 네트워크에 접속해서 데이터를 가져옴

            Log.e("uri", uri);

            try {
                //웹사이트에 접속 (사진이 있는 주소로 접근)
                URL Url = new URL(uri);
                // 웹사이트에 접속 설정
                URLConnection urlcon = Url.openConnection();
                // 연결하시오
                urlcon.connect();
                // 이미지 길이 불러옴
                int imagelength = urlcon.getContentLength();
                Log.e("imagelength", imagelength+"");
                // 스트림 클래스를 이용하여 이미지를 불러옴
                BufferedInputStream bis = new BufferedInputStream(urlcon.getInputStream(), imagelength);
                // 스트림을 통하여 저장된 이미지를 이미지 객체에 넣어줌
                bit = BitmapFactory.decodeStream(bis);
                bis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

    }

}
