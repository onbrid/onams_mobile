package com.onbrid.onams.onams_mobile.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by wonzopein on 15. 4. 1..
 */
public class OnbridPreference {

    private static SharedPreferences preference;


    public static String getString(Context context, String key){
        preference =   (SharedPreferences) PreferenceManager.getDefaultSharedPreferences(context);
        return preference.getString(key, "");
    }

    public static void setString(Context context, String key, String val){
        preference =   (SharedPreferences) PreferenceManager.getDefaultSharedPreferences(context);
        preference.edit().putString(key, val).commit();
    }

    public static int getInt(Context context, String key){
        preference =   (SharedPreferences) PreferenceManager.getDefaultSharedPreferences(context);
        return preference.getInt(key, 0);
    }

    public static float getFloat(Context context, String key){
        preference =   (SharedPreferences) PreferenceManager.getDefaultSharedPreferences(context);
        return preference.getFloat(key, 0f);
    }

    public static boolean getBoolean(Context context, String key){
        preference =   (SharedPreferences) PreferenceManager.getDefaultSharedPreferences(context);
        return preference.getBoolean(key, false);
    }

    public static void setBoolean(Context context, String key, boolean val){
        preference =   (SharedPreferences) PreferenceManager.getDefaultSharedPreferences(context);
        preference.edit().putBoolean(key, val).commit();
    }

}
