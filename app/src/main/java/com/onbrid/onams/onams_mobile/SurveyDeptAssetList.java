package com.onbrid.onams.onams_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.onbrid.onams.onams_mobile.adaptor.DeptAssetListAdaptor;
import com.onbrid.onams.onams_mobile.util.OnbridPreference;
import com.onbrid.onams.onams_mobile.util.OnbridResponseHandler;
import com.onbrid.onams.onams_mobile.util.OnbridRestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class SurveyDeptAssetList extends AppCompatActivity {

    private Toolbar toolbar;

    private ArrayList<JSONObject> assetList;
    private DeptAssetListAdaptor  deptAssetListAdaptor;

    private ListView lvAsset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_dept_asset_list);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        setControll();
        getAssetList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_survey_dept_asset_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        finish();

        return super.onOptionsItemSelected(item);
    }


    private void setControll() {
        lvAsset = (ListView) findViewById(R.id.assetList);
        assetList = new ArrayList<>();
        deptAssetListAdaptor = new DeptAssetListAdaptor(this, assetList);

        lvAsset.setAdapter(deptAssetListAdaptor);
        lvAsset.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplication(), AssetViewActivity.class);
                // intent.putExtras(getIntent().getExtras());
                try {
                    intent.putExtra("ASSETNO", assetList.get(position).getString("ASSETNO"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                startActivity(intent);
            }
        });
    }

    private void getAssetList() {
        String url = "";
        JSONObject param = new JSONObject();
        Intent intent = getIntent();

        switch (intent.getIntExtra("GUBN", -1)) {
            case 0: url = "/ws/pda/summary/ok";     break;
            case 1: url = "/ws/pda/summary/no";     break;
            case 2: url = "/ws/pda/summary/plus";   break;
            case 3: url = "/ws/pda/summary/minus";  break;
        }

        try {
            param.put("UNIVNO"  , OnbridPreference.getString(this, "univNo"));
            param.put("CAMPUSNO", OnbridPreference.getString(this, "campusNo"));
            param.put("PLANNO"  , intent.getStringExtra("PLANNO"));
            param.put("DEPTNO"  , intent.getSerializableExtra("DEPTNO"));
            param.put("PLACENO" , intent.getSerializableExtra("PLACENO"));
            param.put("PRODNO"  , intent.getSerializableExtra("PRODNO"));
            // Log.d("param", param.toString());

            OnbridRestClient.post(this, url, param, new OnbridResponseHandler(this) {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.d("onSuccess", response.toString());
                    // , #{DEPTNO} DEPTNO, NVL(VA.TARGET_CNT,0) TARGET_CNT, NVL(VB.OK_CNT,0) OK_CNT
                    // , NVL(VC.PLUS_CNT,0) PLUS_CNT, NVL(VD.MINUS_CNT,0) MINUS_CNT, NVL(VE.NO_CNT,0) NO_CNT
                    try {

                        int resLeng = response.length();
                        for (int i=0; i<resLeng; i++) {
                            assetList.add((JSONObject) response.get(i));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        deptAssetListAdaptor.notifyDataSetChanged();
                    }
                }
            });


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }



} // class
