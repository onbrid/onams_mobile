package com.onbrid.onams.onams_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

public class SettingActivity extends PreferenceActivity {

    EditTextPreference loginId;
    EditTextPreference passWd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.setting_toolbar);

        addPreferencesFromResource(R.xml.pref_settings);

        LinearLayout root = (LinearLayout)findViewById(android.R.id.list).getParent().getParent().getParent();
        Toolbar bar = (Toolbar) LayoutInflater.from(this).inflate(R.layout.setting_toolbar, root, false);
        root.addView(bar, 0); // insert at top

        bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentFinish();
            }
        });

        this.initPreference();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        intentFinish();
    }

    /**
     * 설정 변경시 업데이트
     */
    private void initPreference() {
        /*
         *  로그인정보
         */
        loginId     = (EditTextPreference) findPreference("userId");
        passWd      = (EditTextPreference) findPreference("userPw");

        setOnPreferenceChange(findPreference("userId"));
        setOnPreferenceChange(findPreference("univNo"));
        setOnPreferenceChange(findPreference("url"));
        setOnPreferenceChange(findPreference("surveyPrefix"));
        setOnPreferenceChange(findPreference("useScanner"));
        setOnPreferenceChange(findPreference("scannerID"));
        setOnPreferenceChange(findPreference("usbBaudRate"));
        setOnPreferenceChange(findPreference("useDatabits"));
        setOnPreferenceChange(findPreference("useStopbits"));
        setOnPreferenceChange(findPreference("useParity"));


    }

    private void setOnPreferenceChange(Preference mPreference) {
        mPreference.setOnPreferenceChangeListener(onPreferenceChangeListener);

        onPreferenceChangeListener.onPreferenceChange(
                mPreference,
                PreferenceManager.getDefaultSharedPreferences(
                        mPreference.getContext()).getString(
                        mPreference.getKey(), ""));
    }


    private Preference.OnPreferenceChangeListener onPreferenceChangeListener = new Preference.OnPreferenceChangeListener() {

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            String stringValue = newValue.toString();

            if (preference instanceof EditTextPreference) {
                preference.setSummary(stringValue);

            } else if (preference instanceof ListPreference) {

                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                preference
                        .setSummary(index >= 0 ? listPreference.getEntries()[index]
                                : null);

            }

            return true;
        }

    };

    private void intentFinish() {

        Intent intent = getIntent();
        intent.putExtra("userId",     loginId.getText());
        intent.putExtra("userPw",     passWd.getText());
        setResult(1, intent);

        finish();
    }
}
