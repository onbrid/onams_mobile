package com.onbrid.onams.onams_mobile.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onbrid.onams.onams_mobile.AssetListActivity;
import com.onbrid.onams.onams_mobile.R;
import com.onbrid.onams.onams_mobile.model.AssetScanItem;

import java.util.ArrayList;

/**
 * Created by wonzopein on 15. 3. 18..
 */
public class AssetListAdaptor extends ArrayAdapter {


    private Context context;

    public AssetListAdaptor(Context context, ArrayList<AssetScanItem> assetList) {
        super(context, 0, assetList);
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final AssetScanItem item = (AssetScanItem) getItem(position);
        ViewHolder viewHolder;

        if( convertView == null ) {
            viewHolder = new ViewHolder();
            convertView =   LayoutInflater.from(getContext()).inflate(R.layout.list_layout_asset, parent, false);

            /* UI 엘리먼트를 매핑 */
            viewHolder.assetIndex   =   (TextView) convertView.findViewById(R.id.assetIndex);
            viewHolder.assetName    =   (TextView) convertView.findViewById(R.id.assetName);
            viewHolder.assetId      =   (TextView) convertView.findViewById(R.id.assetId);
            viewHolder.assetPlace   =   (TextView) convertView.findViewById(R.id.assetPlace);
            viewHolder.assetDept    =   (TextView) convertView.findViewById(R.id.assetDept);
            viewHolder.btnYes       =   (LinearLayout) convertView.findViewById(R.id.lineaLayoutBtnYes);
            viewHolder.btnNo        =   (LinearLayout) convertView.findViewById(R.id.lineaLayoutBtnNo);
            viewHolder.btnClear     =   (LinearLayout) convertView.findViewById(R.id.lineaLayoutBtnClear);

            viewHolder.currentStatus    =   (ImageView) convertView.findViewById(R.id.imageViewCurrentStatus);




            convertView.setTag(viewHolder);

        }else{
            viewHolder  =   (ViewHolder) convertView.getTag();
        }

        /* UI 엘리먼트에 값을 바인딩 */
        viewHolder.assetIndex.setText( (position+1)+". " );
        viewHolder.assetName.setText( item.getName() );
        viewHolder.assetId.setText( item.getId() );
        viewHolder.assetPlace.setText( item.getPlaceName());
        viewHolder.assetDept.setText(item.getDeptName());

        viewHolder.setStatusDraw(item.getStatus());


        viewHolder.btnYes.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Yes!");
                ((AssetListActivity) context).closeItem(position, 0);

            }
        });

        viewHolder.btnNo.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("No!");
                ((AssetListActivity) context).closeItem(position, 1);
            }
        });

        viewHolder.btnClear.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Clear!");
                ((AssetListActivity) context).closeItem(position, -1);
            }
        });

        viewHolder.setStatusDraw( item.getStatus() );

        return convertView;
    }





    private static class ViewHolder {
        TextView assetId;
        TextView assetName;
        TextView assetIndex;

        TextView assetPlace;
        TextView assetDept;

        LinearLayout btnYes;
        LinearLayout btnNo;
        LinearLayout btnClear;

        ImageView currentStatus;

        private void setStatusDraw( int status ){

            if( status == 0 ){
                this.currentStatus.setImageResource( R.drawable.ic_action_accept_light );
            }else if( status == 1 ){
                this.currentStatus.setImageResource( R.drawable.ic_action_cancel_light );
            }else{
                this.currentStatus.setImageResource( R.drawable.ic_action_help_light );
            }

        }

    }

}
