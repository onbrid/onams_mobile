package com.onbrid.onams.onams_mobile.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Enable on 2015-11-27.
 */
public class AssetPlusItem extends AssetItem implements Serializable {

    private String writeTime;

    private String scanDeptNo;
    private String scanDeptName;

    private String scanPlaceNo;
    private String scanPlaceName;


    public AssetPlusItem(String id, String name) {
        super(id, name);
    }

    public AssetPlusItem(JSONObject object) {
        super(object);
        try {
            this.setWriteTime(object.getString("WRITETIME"));
            this.setScanDeptNo(object.getString("SCANDEPTNO"));
            this.setScanDeptName(object.getString("SCANDEPTNAME"));
            this.setScanPlaceNo(object.getString("SCANPLACENO"));
            this.setScanPlaceName(object.getString("SCANPLACENAME"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getWriteTime() {
        return writeTime;
    }

    public void setWriteTime(String writeTime) {
        this.writeTime = writeTime;
    }

    public String getScanDeptNo() {
        return scanDeptNo;
    }

    public void setScanDeptNo(String scanDeptNo) {
        this.scanDeptNo = scanDeptNo;
    }

    public String getScanDeptName() {
        return scanDeptName;
    }

    public void setScanDeptName(String scanDeptName) {
        this.scanDeptName = scanDeptName;
    }

    public String getScanPlaceNo() {
        return scanPlaceNo;
    }

    public void setScanPlaceNo(String scanPlaceNo) {
        this.scanPlaceNo = scanPlaceNo;
    }

    public String getScanPlaceName() {
        return scanPlaceName;
    }

    public void setScanPlaceName(String scanPlaceName) {
        this.scanPlaceName = scanPlaceName;
    }
}
