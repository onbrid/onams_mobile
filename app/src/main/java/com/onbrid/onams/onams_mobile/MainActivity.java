package com.onbrid.onams.onams_mobile;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.onbrid.onams.onams_mobile.util.OnbridPreference;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;

public class MainActivity extends AppCompatActivity {

    Intent intent;

    EditText userId;
    EditText userPw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userId = (EditText) findViewById(R.id.userId);
        userPw = (EditText) findViewById(R.id.passWd);

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLogin();
            }
        });
        findViewById(R.id.btnSetting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingPage();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.e("MainActivity", "onStart=======================");

        if (OnbridPreference.getBoolean(MainActivity.this, "isAutoLogin") == true) {
            userId.setText(OnbridPreference.getString(MainActivity.this, "userId"));
            userPw.setText(OnbridPreference.getString(MainActivity.this, "userPw"));
            this.checkLogin();
        } else {
            userId.setText("");
            userPw.setText("");
        }
    }

    /**
     * 로그인 버튼 클릭이벤트 리스너
     */
    private void checkLogin() {
        // String url  =   OnbridPreference.getString(MainActivity.this, "url");
        // Toast.makeText(getApplicationContext(), url, Toast.LENGTH_LONG).show();

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            try {
                String url = OnbridPreference.getString(MainActivity.this, "url") + "/ws/loginPDA";
                JSONObject param = new JSONObject();
                param.put("UNIVNO", OnbridPreference.getString(MainActivity.this, "univNo"));
                param.put("LOGINID", userId.getText().toString());
                param.put("PASSWD" , userPw.getText().toString());

                JSONArray jsonArray = new JSONArray();
                jsonArray.put(param);

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("DATABLOCK", jsonArray);


                // String _message = new Gson().toJson(param);
                String _message = jsonObject.toString();
                Log.e("_msg", _message);

                ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(getParent(), url, entity, "application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        // super.onSuccess(statusCode, headers, response);
                        try {
                            // Log.e("checkLogin", response.toString());
                            // {"data":{"CAMPUSNO":"01","DEPTNO":"0001","EMPNAME":"관리자","UNIVNO":"0001","CAMPUSNAME":"OnBrid-대학","LOGINID":"1001",
                            //          "DEPTNAME":"기획 관리부","GROUPNAME":"관리자","EMPNO":"1001","ADMINGUBN":1,"GROUPNO":"0001","UNIVNAME":"OnBrid-대학"}}
                            if (response.length() < 1) {
                                Toast.makeText(MainActivity.this, "로그인 정보를 확인하세요.", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            OnbridPreference.setString(getApplicationContext(), "campusNo"      , response.getString("CAMPUSNO"));
                            OnbridPreference.setString(getApplicationContext(), "deptNo"        , response.getString("DEPTNO"));
                            OnbridPreference.setString(getApplicationContext(), "deptName"      , response.getString("DEPTNAME"));
                            OnbridPreference.setString(getApplicationContext(), "loginEmp"      , response.getString("EMPNO"));
                            OnbridPreference.setString(getApplicationContext(), "loginEmpName"  , response.getString("EMPNAME"));
                            OnbridPreference.setString(getApplicationContext(), "adminGubn"     , response.getString("ADMINGUBN"));
                            
                            OnbridPreference.setString(getApplicationContext(), "planNo"        , response.getString("PLANNO"));
                            OnbridPreference.setString(getApplicationContext(), "planName"      , response.getString("PLANNAME"));

                            OnbridPreference.setString(getApplicationContext(), "userId"      , userId.getText().toString());
                            //OnbridPreference.setString(getApplicationContext(), "userPw"      , userPw.getText().toString());

                            intent = new Intent(MainActivity.this, DrawerActivity.class);
                            startActivity(intent);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Toast.makeText(MainActivity.this, "로그인 정보를 확인하세요.", Toast.LENGTH_SHORT).show();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(getApplicationContext(), "네트워크 연결상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
        }

    }




    /**
     * 세팅버튼 클릭이벤트 리스너
     */
    private void settingPage() {
        intent = new Intent(MainActivity.this, SettingActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        userId.setText(data.getStringExtra("userId"));
        userPw.setText(data.getStringExtra("userPw"));

    }
}
