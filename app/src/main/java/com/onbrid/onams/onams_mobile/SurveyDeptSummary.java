package com.onbrid.onams.onams_mobile;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.onbrid.onams.onams_mobile.adaptor.DeptProdListAdaptor;
import com.onbrid.onams.onams_mobile.util.OnbridPreference;
import com.onbrid.onams.onams_mobile.util.OnbridResponseHandler;
import com.onbrid.onams.onams_mobile.util.OnbridRestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class SurveyDeptSummary extends AppCompatActivity {

    private Toolbar toolbar;

    private JSONObject param;

    private ArrayList<JSONObject> prodList;
    private DeptProdListAdaptor   deptProdListAdaptor;

    private int      gubn = -1;

    private TextView okCnt;
    private TextView noCnt;
    private TextView plusCnt;
    private TextView minusCnt;
    private TextView tvNoData;
    private TextView tvTitle;
    private ListView lvProd;

    /**
     * 온클릭 리스너
     */
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            prodList.clear();
            deptProdListAdaptor.notifyDataSetChanged();

            String url = "";
            switch (v.getId()) {
                case R.id.okCnt    :
                    setText(tvTitle, "물품구분별 #확인# 재물 집계 목록");
                    if(Integer.parseInt(okCnt.getText().toString())    < 1) { return; }
                    url = "/ws/pda/summary/prodok";
                    gubn = 0;
                    break;
                case R.id.noCnt    :
                    setText(tvTitle, "물품구분별 #미확인# 재물 집계 목록");
                    if(Integer.parseInt(noCnt.getText().toString())    < 1) { return; }
                    url = "/ws/pda/summary/prodno";
                    gubn = 1;
                    break;
                case R.id.plusCnt  :
                    setText(tvTitle, "물품구분별 #추가# 재물 집계 목록");
                    if(Integer.parseInt(plusCnt.getText().toString())  < 1) { return; }
                    url = "/ws/pda/summary/prodplus";
                    gubn = 2;
                    break;
                case R.id.minusCnt :
                    setText(tvTitle, "물품구분별 #타실# 재물 집계 목록");
                    if(Integer.parseInt(minusCnt.getText().toString()) < 1) { return; }
                    url = "/ws/pda/summary/prodminus";
                    gubn = 3;
                    break;
            }

            getProdList(url);

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_dept_summary);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        setControll();

        getSummaryCount();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_survey_dept_summary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        finish();

        return super.onOptionsItemSelected(item);
    }




    /**
     * 컨트롤 연결한다
     */
    private void setControll() {

        okCnt     = (TextView) findViewById(R.id.okCnt);
        noCnt     = (TextView) findViewById(R.id.noCnt);
        plusCnt   = (TextView) findViewById(R.id.plusCnt);
        minusCnt  = (TextView) findViewById(R.id.minusCnt);
        tvNoData  = (TextView) findViewById(R.id.tvNoData);
        tvTitle   = (TextView) findViewById(R.id.tvTitle);
        lvProd    = (ListView) findViewById(R.id.prodList);
        // TextView에 언더라인 설정
        okCnt   .setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        noCnt   .setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        plusCnt .setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        minusCnt.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        okCnt     .setOnClickListener(onClickListener);
        noCnt     .setOnClickListener(onClickListener);
        plusCnt   .setOnClickListener(onClickListener);
        minusCnt  .setOnClickListener(onClickListener);

        initControll();
    }
    /**
     * 컨트롤 초기화한다.
     */
    private void initControll() {

        param = new JSONObject();
        try {
            Intent intent = getIntent();
            param.put("UNIVNO", OnbridPreference.getString(this, "univNo"));
            param.put("CAMPUSNO", OnbridPreference.getString(this, "campusNo"));
            param.put("PLANNO", intent.getStringExtra("PLANNO"));
            param.put("DEPTNO", intent.getSerializableExtra("DEPTNO"));
            param.put("PLACENO", intent.getSerializableExtra("PLACENO"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        initListView();
    }

    /**
     * 리스트뷰를 초기화한다.
     */
    private void initListView() {
        prodList = new ArrayList<>();
        deptProdListAdaptor = new DeptProdListAdaptor(this, prodList);

        lvProd.setAdapter(deptProdListAdaptor);

        lvProd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Log.d("PRODNO", prodList.get(position).toString());
                // 재물목록 인텐트 호출
                Intent intent = new Intent(getApplication(), SurveyDeptAssetList.class);
                intent.putExtras(getIntent().getExtras());
                intent.putExtra("GUBN", gubn);
                try {
                    intent.putExtra("PRODNO", prodList.get(position).getString("PRODNO"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                startActivity(intent);

            }
        });

    }
    /**
     * EditText, TextView 에 text(내용)을 셋한다.
     * @param item
     * @param val
     */
    private void setText(TextView item, Object val) {
        item.setText(val.toString());
    }

    /**
     * 상단 토탈 집계를 조회한다.
     */
    private void getSummaryCount() {
        String url = "/ws/pda/summary/total";

        try {
            // Log.d("param", param.toString());

            OnbridRestClient.post(this, url, param, new OnbridResponseHandler(this) {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.d("onSuccess", response.toString());
                    // , #{DEPTNO} DEPTNO, NVL(VA.TARGET_CNT,0) TARGET_CNT, NVL(VB.OK_CNT,0) OK_CNT
                    // , NVL(VC.PLUS_CNT,0) PLUS_CNT, NVL(VD.MINUS_CNT,0) MINUS_CNT, NVL(VE.NO_CNT,0) NO_CNT
                    try {
                        setText((TextView) findViewById(R.id.targetCnt), response.getString("TARGET_CNT"));
                        setText(okCnt    , response.getString("OK_CNT"));
                        setText(noCnt    , response.getString("NO_CNT"));
                        setText(plusCnt  , response.getString("PLUS_CNT"));
                        setText(minusCnt , response.getString("MINUS_CNT"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 상단 집계 클릭시 리스트뷰 내용을 조회한다.
     * @param url
     */
    private void getProdList(String url) {

        try {
            // Log.d("param", param.toString());

            OnbridRestClient.post(this, url, param, new OnbridResponseHandler(this) {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.d("onSuccess", response.toString());
                    // [{"PLANNO":"20150024","CAMPUSNO":"01","DEPTNO":"0015","PRODNO":"0013","CNT":"1","PRODNAME":"다용도앰프","UNIVNO":"0001"}]
                    try {
                        if (response.length() > 0) {
                            tvNoData.setVisibility(View.GONE);
                            tvTitle.setVisibility(View.VISIBLE);
                            findViewById(R.id.underLine).setVisibility(View.VISIBLE);
                            int resLeng = response.length();
                            for (int i=0; i<resLeng; i++) {
                                prodList.add((JSONObject)response.get(i));
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        deptProdListAdaptor.notifyDataSetChanged();
                    }
                }
            });


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }





} // class
