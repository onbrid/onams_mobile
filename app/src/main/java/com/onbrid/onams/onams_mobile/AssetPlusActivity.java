package com.onbrid.onams.onams_mobile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.onbrid.onams.onams_mobile.adaptor.SurveyPlusListAdaptor;
import com.onbrid.onams.onams_mobile.model.AssetPlusItem;
import com.onbrid.onams.onams_mobile.util.OnbridPreference;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;

public class AssetPlusActivity extends ActionBarActivity {


    private Toolbar toolbar;

    private ArrayList<AssetPlusItem> plusDataList;
    private ListView listView;
    private SurveyPlusListAdaptor surveyPlusListAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_plus);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.listview);

        // 추가 리스트 조회
        this.selectSurveyDeptPlusList();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_asset_plus, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_plus :
                Intent intent = new Intent(this, AssetPlusInsertActivity.class);

                intent.putExtra("PLANNO"        , getIntent().getSerializableExtra("PLANNO"));
                intent.putExtra("SCANDEPTNO"    , getIntent().getSerializableExtra("SCANDEPTNO"));
                intent.putExtra("SCANDEPTNAME"  , getIntent().getSerializableExtra("SCANDEPTNAME"));
                intent.putExtra("SCANPLACENO"   , getIntent().getSerializableExtra("SCANPLACENO"));
                intent.putExtra("SCANPLACENAME" , getIntent().getSerializableExtra("SCANPLACENAME"));
                startActivity(intent);
                break;
            default:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }




    private void selectSurveyDeptPlusList() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            try {
                this.showProgress(true);
                String url = OnbridPreference.getString(AssetPlusActivity.this, "url") + "/ws/sa/surveyDept/selectSurveyDeptSummaryPlusList";

                Map param = new HashMap();
                param.put("UNIVNO"           , OnbridPreference.getString(AssetPlusActivity.this, "univNo"));
                param.put("CAMPUSNO"         , OnbridPreference.getString(AssetPlusActivity.this, "campusNo"));
                param.put("PLANNO"           , getIntent().getSerializableExtra("PLANNO"));
                param.put("PLACENO"          , getIntent().getSerializableExtra("SCANPLACENO"));
                param.put("DEPTNO"           , getIntent().getSerializableExtra("SCANDEPTNO"));
                param.put("LOGINEMP"         , OnbridPreference.getString(AssetPlusActivity.this, "loginEmp"));
                Log.e("PlusList.param", param.toString());

                String _message = new Gson().toJson(param);

                ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

                plusDataList = new ArrayList<AssetPlusItem>();

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(getParent(), url, entity, "application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        // super.onSuccess(statusCode, headers, response);
                        try {
                            Log.e("PlusList", response.toString());
                            JSONObject item;
                            for (int i=0; i<response.length(); i++) {
                                item = (JSONObject) response.get(i);

                                Log.e("PlusList.item", (new AssetPlusItem(item)).getScanDeptName());

                                plusDataList.add(new AssetPlusItem(item));


                            }
                            setListViewAdaptor(plusDataList);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        //super.onFailure(statusCode, headers, throwable, errorResponse);

                        Log.e("PlusList.onFailure", errorResponse.toString());

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                this.showProgress(false);
            }

        } else {
            Toast.makeText(getApplicationContext(), "네트워크 연결상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
        }


    }


    private void setListViewAdaptor( ArrayList arrList ) {
        surveyPlusListAdaptor = new SurveyPlusListAdaptor(AssetPlusActivity.this, arrList);
        listView.setAdapter(surveyPlusListAdaptor);

                            /* 추가조사 재물 리스트 클릭 이벤트 */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setTitle("추가조사 재물 삭제");

                builder.setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AssetPlusItem item = (AssetPlusItem) surveyPlusListAdaptor.getItem(position);

                        // 삭제
                        deleteSurveyPlus(item, position);
                    }
                });
                builder.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
    }


    private void deleteSurveyPlus(final AssetPlusItem item, final int position) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            try {
                this.showProgress(true);
                String url = OnbridPreference.getString(AssetPlusActivity.this, "url") + "/ws/sa/surveyDept/deleteSurveyDeptPlusAsset";

                Map param = new HashMap();
                param.put("UNIVNO"           , OnbridPreference.getString(AssetPlusActivity.this, "univNo"));
                param.put("CAMPUSNO"         , OnbridPreference.getString(AssetPlusActivity.this, "campusNo"));
                param.put("PLANNO"           , getIntent().getSerializableExtra("PLANNO"));
                param.put("ASSETNO"          , item.getId());
                param.put("WRITETIME"        , item.getWriteTime());
                param.put("LOGINEMP"         , OnbridPreference.getString(AssetPlusActivity.this, "loginEmp"));
                Log.e("deletePlus.param", param.toString());

                String _message = new Gson().toJson(param);

                ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(getParent(), url, entity, "application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        // super.onSuccess(statusCode, headers, response);
                        try {
                            Log.e("deleteSurveyPlus", response.toString());

                            plusDataList.remove(position);
                            listView.clearChoices();
                            surveyPlusListAdaptor.notifyDataSetChanged();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        //super.onFailure(statusCode, headers, throwable, errorResponse);

                        Log.e("deletePlus.onFailure", errorResponse.toString());

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                this.showProgress(false);
            }

        } else {
            Toast.makeText(getApplicationContext(), "네트워크 연결상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
        }

    }









    /**
     * 프로그레스 바 설정
     * @param show
     */
    ProgressDialog progressDialog;
    public void showProgress(boolean show ){

        if( show ){

            try{
                progressDialog = new ProgressDialog( this );
                progressDialog.setCancelable(false);
                progressDialog.setMessage("잠시만 기다려주세요.");
                progressDialog.show();
            }catch(Exception e){

            }


        }else{

            if( progressDialog != null ){
                progressDialog.dismiss();
            }

        }

    }


}
