package com.onbrid.onams.onams_mobile.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Enable on 2016-03-10.
 */
public class MarketItem implements Serializable {

    private String workNo;
    private String assetNo;
    private String assetName;
    private String writeDate;

    private String reqDeptNo;
    private String reqDeptName;
    private String reqEmpNo;
    private String reqEmpName;

    private String fromDeptNo;
    private String fromDeptName;
    private String fromPlaceNo;
    private String fromPlaceName;
    private String fromEmpNo;
    private String fromEmpName;
    private String fromUseEmpName;
    private String toDeptNo;
    private String toDeptName;
    private String toPlaceNo;
    private String toPlaceName;
    private String toEmpNo;
    private String toEmpName;
    private String toUseEmpName;

    private int    returngubn;
    private String mngTel;
    private String mngEmail;

    private int confState = -1;

    // {"REQEMPNAME":"관리자","TOPLACENO":null,"WRITEEMPNAME":"관리자","CAMPUSNO":"1","REQEMPNO":"1001","CONFSTATE":1,
    // "MNGEMAIL":"email@email.com","ASSETNO":"225100100000014","TOEMPNAME":"관리자","FROMPLACENAME":"관재팀 불용처리 대기중",
    // "ASSETNAME":"의자 대회전의자(철재)","READCNT":0,"REQDEPTNAME":"관재팀","TOTALROW":9,"FROMEMPNO":"010764","PAGENUM":1,
    // "MODELNAME":null,"UNIVNO":"0001","TOPLACENAME":null,"MNGTEL":"02-1234-5678","REQDEPTNO":"1104510","FROMDEPTNAME":"관재팀",
    // "WORKNO":"2016030013","WRITEDATE":"20160309","TODEPTNAME":"관재팀","WRITEDEPTNAME":"관재팀","TODEPTNO":"1104510",
    // "FROMPLACENO":"X01451","TOUSEEMPNAME":null,"FROMDEPTNO":"1104510","RETURNGUBN":1,"FROMUSEEMPNAME":null,"TOTALPAGE":1,"FROMEMPNAME":"김향란","TOEMPNO":"1001"}
    public MarketItem(JSONObject item) {
        try {
            setAssetNo(item.getString("ASSETNO"));
            setAssetName(item.getString("ASSETNAME"));
            setWriteDate(item.getString("WRITEDATE"));
            setFromDeptName(item.getString("FROMDEPTNAME"));
            setFromPlaceName(item.getString("FROMPLACENAME"));
            setWorkNo(item.getString("WORKNO"));
            if (item.get("CONFSTATE") != null) {
                setConfState(item.getInt("CONFSTATE"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getWorkNo() {
        return workNo;
    }

    public void setWorkNo(String workNo) {
        this.workNo = workNo;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getWriteDate() {
        return writeDate;
    }

    public void setWriteDate(String writeDate) {

        StringBuffer buffer = new StringBuffer(writeDate);
        buffer.insert(4, "-").insert(7, "-");

        this.writeDate = buffer.toString();
    }

    public String getReqDeptNo() {
        return reqDeptNo;
    }

    public void setReqDeptNo(String reqDeptNo) {
        this.reqDeptNo = reqDeptNo;
    }

    public String getReqEmpNo() {
        return reqEmpNo;
    }

    public void setReqEmpNo(String reqEmpNo) {
        this.reqEmpNo = reqEmpNo;
    }

    public String getFromDeptNo() {
        return fromDeptNo;
    }

    public void setFromDeptNo(String fromDeptNo) {
        this.fromDeptNo = fromDeptNo;
    }

    public String getFromPlaceNo() {
        return fromPlaceNo;
    }

    public void setFromPlaceNo(String fromPlaceNo) {
        this.fromPlaceNo = fromPlaceNo;
    }

    public String getFromEmpNo() {
        return fromEmpNo;
    }

    public void setFromEmpNo(String fromEmpNo) {
        this.fromEmpNo = fromEmpNo;
    }

    public String getFromUseEmpName() {
        return fromUseEmpName;
    }

    public void setFromUseEmpName(String fromUseEmpName) {
        this.fromUseEmpName = fromUseEmpName;
    }

    public String getToDeptNo() {
        return toDeptNo;
    }

    public void setToDeptNo(String toDeptNo) {
        this.toDeptNo = toDeptNo;
    }

    public String getToPlaceNo() {
        return toPlaceNo;
    }

    public void setToPlaceNo(String toPlaceNo) {
        this.toPlaceNo = toPlaceNo;
    }

    public String getToEmpNo() {
        return toEmpNo;
    }

    public void setToEmpNo(String toEmpNo) {
        this.toEmpNo = toEmpNo;
    }

    public String getToUseEmpName() {
        return toUseEmpName;
    }

    public void setToUseEmpName(String toUseEmpName) {
        this.toUseEmpName = toUseEmpName;
    }

    public int getReturngubn() {
        return returngubn;
    }

    public void setReturngubn(int returngubn) {
        this.returngubn = returngubn;
    }

    public String getMngTel() {
        return mngTel;
    }

    public void setMngTel(String mngTel) {
        this.mngTel = mngTel;
    }

    public String getMngEmail() {
        return mngEmail;
    }

    public void setMngEmail(String mngEmail) {
        this.mngEmail = mngEmail;
    }


    public String getReqDeptName() {
        return reqDeptName;
    }

    public void setReqDeptName(String reqDeptName) {
        this.reqDeptName = reqDeptName;
    }

    public String getReqEmpName() {
        return reqEmpName;
    }

    public void setReqEmpName(String reqEmpName) {
        this.reqEmpName = reqEmpName;
    }

    public String getFromDeptName() {
        return fromDeptName;
    }

    public void setFromDeptName(String fromDeptName) {
        this.fromDeptName = fromDeptName.equalsIgnoreCase("null")? "-": fromDeptName;
    }

    public String getFromPlaceName() {
        return fromPlaceName;
    }

    public void setFromPlaceName(String fromPlaceName) {
        this.fromPlaceName = fromPlaceName.equalsIgnoreCase("null")? "-":fromPlaceName;
    }

    public String getFromEmpName() {
        return fromEmpName;
    }

    public void setFromEmpName(String fromEmpName) {
        this.fromEmpName = fromEmpName;
    }

    public String getToDeptName() {
        return toDeptName;
    }

    public void setToDeptName(String toDeptName) {
        this.toDeptName = toDeptName;
    }

    public String getToPlaceName() {
        return toPlaceName;
    }

    public void setToPlaceName(String toPlaceName) {
        this.toPlaceName = toPlaceName;
    }

    public String getToEmpName() {
        return toEmpName;
    }

    public void setToEmpName(String toEmpName) {
        this.toEmpName = toEmpName;
    }

    public int getConfState() {
        return confState;
    }

    public void setConfState(int confState) {
        this.confState = confState;
    }
}
