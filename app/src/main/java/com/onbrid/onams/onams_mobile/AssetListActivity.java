package com.onbrid.onams.onams_mobile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;
import com.onbrid.onams.onams_mobile.aa.BaseSwipeListViewListener;
import com.onbrid.onams.onams_mobile.aa.SwipeListView;
import com.onbrid.onams.onams_mobile.adaptor.AssetListAdaptor;
import com.onbrid.onams.onams_mobile.model.Asset;
import com.onbrid.onams.onams_mobile.model.AssetScanItem;
import com.onbrid.onams.onams_mobile.model.Code;
import com.onbrid.onams.onams_mobile.util.OnbridPreference;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;

/**
 * 부서재물조사 작업화면 있음/없음 처리.
 */
public class AssetListActivity extends ActionBarActivity {

    SwipeListView listViewAsset;
    AssetListAdaptor assetListAdaptor;

    ArrayList<Map>  files;

    TextView listCount;

    int currentPage =   0;
    int totalPage   =   0;

    // 없음에 대한 사유
    int surveyAssetState = -1;
    String bigo = "";

    Map searchOption;


    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_list);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listViewAsset   =   (SwipeListView) findViewById(R.id.listViewAsset);

        ArrayList<AssetScanItem> data = (ArrayList<AssetScanItem>) getIntent().getSerializableExtra("items");

        if(data.size() != 0 ){
            currentPage =   data.get(0).getCurrentPage();
            totalPage   =   data.get(0).getTotalPage();
            //getSupportActionBar().setTitle("재물목록(" + data.get(0).getTotalCount() + ")");
        }


        this.initSearchOption();
        this.initListview(data);
    }

    private void initSearchOption(){
        searchOption    =   new HashMap();
        try{

            searchOption.put("UNIVNO"    , OnbridPreference.getString(AssetListActivity.this, "univNo"));
            searchOption.put("CAMPUSNO"    , OnbridPreference.getString(AssetListActivity.this, "campusNo"));
            searchOption.put("PAGENUM"   , currentPage);
            searchOption.put("PAGEROW"   , 20);
            searchOption.put("PLANNO"    , getIntent().getSerializableExtra("PLANNO") );
            searchOption.put("LOGINEMP"  , getIntent().getSerializableExtra("LOGINEMP") );
            searchOption.put("PLACENO"   , getIntent().getSerializableExtra("PLACENO") );
            searchOption.put("DEPTNO"    , getIntent().getSerializableExtra("DEPTNO") );

        }catch (Exception e){

        }
    }

    private void initListview( ArrayList<AssetScanItem> data ){

        assetListAdaptor    =   new AssetListAdaptor(this, data);

        listViewAsset.setAdapter(assetListAdaptor);

        listViewAsset.setSwipeListViewListener( new BaseSwipeListViewListener(){
            @Override
            public void onClickFrontView(int position) {
                super.onClickFrontView(position);

                //listViewAsset.setItemChecked(position, false);
                viewDetail((Asset) assetListAdaptor.getItem(position), position);

            }

            @Override
            public void onClickBackView(int position) {
                super.onClickBackView(position);
                listViewAsset.closeAnimate(position);
            }

            @Override
            public void onDismiss(int[] reverseSortedPositions) {
                super.onDismiss(reverseSortedPositions);
                /*
                for (int position : reverseSortedPositions) {
                    assetListAdaptor.remove( assetListAdaptor.getItem(position) );
                }
                assetListAdaptor.notifyDataSetChanged();
                */


            }
        });

        listViewAsset.setAdapter(assetListAdaptor);
        listViewAsset.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                System.out.println("itemClick");

                listViewAsset.setItemChecked(position, false);
                viewDetail((AssetScanItem) assetListAdaptor.getItem(position), position);

                /*
                if( (position+1)%2 == 0 ){
                    listViewAsset.setItemChecked(position, false);
                    viewDetail((Asset) assetListAdaptor.getItem(position));
                }else{
                    Asset item = (Asset) assetListAdaptor.getItem(position);
                    AssetDetailDialog assetDetailDialog = new AssetDetailDialog( parent.getContext() );
                    assetDetailDialog.setTitle("재물상세정보");
                    assetDetailDialog.setAsset(item);
                    assetDetailDialog.show();
                }
                */

            }
        });


        listViewAsset.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                //System.out.println(scrollState);
            }


            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                //System.out.println( "loading : "+ loading +" || " + firstVisibleItem + "/" + visibleItemCount + "/"+ totalItemCount + " || " + currentPage+"/"+totalPage);

                if (!loading && currentPage < totalPage && (firstVisibleItem + visibleItemCount) >= (totalItemCount - visibleItemCount)) {
                    loading = true;
                    currentPage++;
                    searchOption.put("PAGENUM", currentPage);
                    //new DataManager().execute();
                    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                    if (networkInfo != null && networkInfo.isConnected()) {

                        try {
                            String url = OnbridPreference.getString(AssetListActivity.this, "url") + "/ws/sa/surveyDept/selectSurveyList";

                            String _message = new Gson().toJson(searchOption);
                            ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

                            final ArrayList<Asset> data = new ArrayList<Asset>();

                            AsyncHttpClient client = new AsyncHttpClient();
                            client.post(getParent(), url, entity, "application/json", new JsonHttpResponseHandler() {
                                @Override
                                public void onStart() {
                                    super.onStart();
                                    showProgress(true);
                                }

                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                    //super.onSuccess(statusCode, headers, response);
                                    try {
                                        JSONArray jsonArray = (JSONArray) response.getJSONArray("list");
                                        System.out.println("jsonArray : " + jsonArray.length());


                                        JSONObject item;
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            item = (JSONObject) jsonArray.get(i);
                                            data.add(new AssetScanItem(item));
                                        }


                                        //System.out.println( ((ArrayList<AssetScanItem>) o).size() +"") ;

                                        assetListAdaptor.addAll(data);
                                        assetListAdaptor.notifyDataSetChanged();
                                        //showProgress(false);
                                        //loading = false;


                                    } catch (Exception e) {

                                    }
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                    //super.onFailure(statusCode, headers, responseString, throwable);
                                    Toast.makeText(getApplicationContext(), "데이터가 없습니다.", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onFinish() {
                                    super.onFinish();
                                    showProgress(false);
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "네트워크 연결상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
                    }

                } else {


                }

            }
        });
    }

    public void closeItem(int position, int isExsist ){

        if (isExsist == 1) {
            this.showCustomDialog( position, isExsist );
        } else {

            if (((AssetScanItem) assetListAdaptor.getItem(position)).getStatus() == isExsist) {
                //Log.e("closeItem", "같음~~" + ((AssetScanItem) assetListAdaptor.getItem(position)).getStatus() + "/" + isExsist);
                listViewAsset.closeAnimate(position);
                return;
            }

            this.updateSurvetAsset(position, isExsist, isExsist, "");

        }

        // assetListAdaptor.notifyDataSetChanged();
        // listViewAsset.closeAnimate(position);
    }

    // 부서재물조사 있음/없음 통신을한다.
    private void updateSurvetAsset( int position, int isExsist, int assetState, String bigo ) {
        final int posi = position;
        final int isExs = isExsist;

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            try {
                String url = OnbridPreference.getString(AssetListActivity.this, "url") + "/ws/sa/surveyDept/updateReadGubn";

                Map param = new HashMap();
                param.put("UNIVNO"           , OnbridPreference.getString(AssetListActivity.this, "univNo"));
                param.put("CAMPUSNO"         , OnbridPreference.getString(AssetListActivity.this, "campusNo"));
                param.put("PLANNO"           , getIntent().getSerializableExtra("PLANNO"));
                param.put("ASSETNO"          , ((AssetScanItem) assetListAdaptor.getItem(position)).getId());
                param.put("PLACENO"          , ((AssetScanItem) assetListAdaptor.getItem(position)).getPlaceNo());
                param.put("DEPTNO"           , ((AssetScanItem) assetListAdaptor.getItem(position)).getDeptNo());
                param.put("USEEMPNO"         , "");
                param.put("ACTIONGUBN"       , 1);
                param.put("READGUBN"         , 4);
                param.put("SURVEYGUBN"       , (getIntent().getSerializableExtra("SCANPLACENO").equals(""))? 1 : 0); // 장소기반 부서기반.
                param.put("DEPTSURVEYGUBN"   , (isExsist == -1)?   null : isExsist); // 있음.없음.미확ㄷ인
                param.put("SURVEYASSETSTATE" , (assetState == -1)? null : assetState); // 0정상 1추가등등 11:반출, 12:망실, 13:수리, 14:tag파손, 19:기타
                param.put("SCANPLACENO"      , (getIntent().getSerializableExtra("SCANPLACENO").equals(""))? ((AssetScanItem) assetListAdaptor.getItem(position)).getPlaceNo() : getIntent().getSerializableExtra("SCANPLACENO"));
                param.put("SCANDEPTNO"       , getIntent().getSerializableExtra("SCANDEPTNO"));
                param.put("SCANEMPNO"        , OnbridPreference.getString(AssetListActivity.this, "loginEmp"));
                param.put("BIGO"             , bigo);
                param.put("LOGINEMP"         , OnbridPreference.getString(AssetListActivity.this, "loginEmp"));
                // Log.e("closeItem.param", param.toString());

                String _message = new Gson().toJson(param);

                ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(getParent(), url, entity, "application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        // super.onSuccess(statusCode, headers, response);
                        try {
                            Log.e("closeItem", response.toString());

                            ((AssetScanItem) assetListAdaptor.getItem(posi)).setStatus(isExs);
                            assetListAdaptor.notifyDataSetChanged();
                            listViewAsset.closeAnimate(posi);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(getApplicationContext(), "네트워크 연결상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
        }




    }

    private void showCustomDialog(int position, int isExsist) {
        final int posi = position;
        final int isExs = isExsist;

        //Dialog에서 보여줄 입력화면 View 객체 생성 작업
        //Layout xml 리소스 파일을 View 객체로 부불려 주는(inflate) LayoutInflater 객체 생성
        LayoutInflater inflater=getLayoutInflater();

        //res폴더>>layout폴더>>dialog_addmember.xml 레이아웃 리소스 파일로 View 객체 생성
        //Dialog의 listener에서 사용하기 위해 final로 참조변수 선언
        final View dialogView= inflater.inflate(R.layout.dialog_deptsurvey_no, null);

        //멤버의 세부내역 입력 Dialog 생성 및 보이기
        AlertDialog.Builder buider= new AlertDialog.Builder(this); //AlertDialog.Builder 객체 생성
        buider.setTitle("사유 설정"); //Dialog 제목
        buider.setIcon(android.R.drawable.ic_menu_add); //제목옆의 아이콘 이미지(원하는 이미지 설정)
        buider.setView(dialogView); //위에서 inflater가 만든 dialogView 객체 세팅 (Customize)
        buider.setPositiveButton("저장", new DialogInterface.OnClickListener() {
            //Dialog에 "Complite"라는 타이틀의 버튼을 설정
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

                //dialogView 객체 안에서 NAME을 입력받는 EditText 객체 찾아오기(주의: dialaogView에서 find 해야함)
                EditText edit_Bigo= (EditText)dialogView.findViewById(R.id.dialog_edit);

                //dialogView 객체 안에서 NATION을 입력받는 RadioGroup 객체 찾아오기
                RadioGroup rg= (RadioGroup)dialogView.findViewById(R.id.dialog_rg);

                if ( rg.getCheckedRadioButtonId() < 0 ) {
                    Toast.makeText(AssetListActivity.this, "사유를 선택하세요.", Toast.LENGTH_SHORT).show();
                } else {
                    //EditText에 입력된 이름 얻어오기
                    bigo= edit_Bigo.getText().toString();
                    //선택된 RadioButton의 ID를 RadioGroup에게 얻어오기
                    int checkedId= rg.getCheckedRadioButtonId();

                    //Check 된 RadioButton의 ID로 라디오버튼 객체 찾아오기
                    RadioButton rb= (RadioButton)rg.findViewById(checkedId);
                    String nation= rb.getText().toString();//RadionButton의 Text 얻어오기
                    // 11:반출, 12:망실, 13:수리, 14:tag파손, 19:기타
                    int assetState = -1;
                    switch (nation) {
                        case "반출"    : assetState = 11; break;
                        case "망실"    : assetState = 12; break;
                        case "수리"    : assetState = 13; break;
                        case "tag파손" : assetState = 14; break;
                        case "기타"    : assetState = 19; break;
                    }
                    Toast.makeText(AssetListActivity.this, checkedId+ "/"+nation+"/"+bigo, Toast.LENGTH_LONG).show();

                    updateSurvetAsset(posi, isExs, assetState, edit_Bigo.getText().toString());
                }

            }
        });
        buider.setNegativeButton("취소", new DialogInterface.OnClickListener() {
            //Dialog에 "Cancel"이라는 타이틀의 버튼을 설정

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                //멤버 정보의 입력을 취소하고 Dialog를 종료하는 작업
                //취소하였기에 특별한 작업은 없고 '취소'했다는 메세지만 Toast로 출력
                Toast.makeText(AssetListActivity.this, "작업을 취소합니다", Toast.LENGTH_SHORT).show();
            }
        });

        //설정한 값으로 AlertDialog 객체 생성
        AlertDialog dialog=buider.create();

        //Dialog의 바깥쪽을 터치했을 때 Dialog를 없앨지 설정
        dialog.setCanceledOnTouchOutside(false);//없어지지 않도록 설정

        //Dialog 보이기
        dialog.show();

    }





    public void onDialogResult( int position, Code item ) {
         Toast.makeText(AssetListActivity.this, "onDialogResult", Toast.LENGTH_SHORT).show();
         System.out.println(position + " : " + item.getNo() + "/" + item.getName());
    }




    private boolean loading = false;

    private void addTestData(int startPosition){

        ArrayList<Asset> data = new ArrayList<>();

        for( int i=0 ; i<10 ; i++ ){
            data.add( new Asset("id_"+ (i+startPosition), "name_"+(i+startPosition) ));
        }


        assetListAdaptor.addAll(data);
        assetListAdaptor.notifyDataSetChanged();
        listCount.setText(assetListAdaptor.getCount() + "");

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_asset_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search :
                //Log.e("onOptionsItemSelected", R.id.action_search + "/" + item.getItemId());
                // 추가 버튼 작업.
                // 추가된 리스트를 보여주고 추가시킨다??
                Intent intent = new Intent(this, AssetPlusActivity.class);

                intent.putExtra("PLANNO"       , getIntent().getSerializableExtra("PLANNO"));
                intent.putExtra("SCANDEPTNO"   , getIntent().getSerializableExtra("SCANDEPTNO"));
                intent.putExtra("SCANDEPTNAME" , getIntent().getSerializableExtra("SCANDEPTNAME"));
                intent.putExtra("SCANPLACENO"  , getIntent().getSerializableExtra("SCANPLACENO"));
                intent.putExtra("SCANPLACENAME", getIntent().getSerializableExtra("SCANPLACENAME"));
                startActivity(intent);
                break;
            default:
                //Log.e("onOptionsItemSelected", "item.getItemId() = " + item.getItemId());
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    public void viewDetail( Asset item, int position ) {

        Intent intent = new Intent(this, AssetDetailActivity.class);
        intent.putExtra("asset", item);
        intent.putExtra("position", position);

        intent.putExtra("PLANNO"     , getIntent().getSerializableExtra("PLANNO"));
        intent.putExtra("SCANDEPTNO" , getIntent().getSerializableExtra("SCANDEPTNO"));
        intent.putExtra("SCANPLACENO", getIntent().getSerializableExtra("SCANPLACENO"));
        //startActivity(intent);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1: // viewDetail result
                if (resultCode == RESULT_OK) {
                    ((AssetScanItem) assetListAdaptor.getItem(data.getIntExtra("position", -1))).setStatus(data.getIntExtra("surveyState", -1));
                    assetListAdaptor.notifyDataSetChanged();
                }
                break;
        }

    }

    public void btnClick(View v){
        Intent intent = new Intent();
        intent.putExtra("a", "으헐헐헐");
        setResult(RESULT_OK, intent);
        finish();
    }



    ProgressDialog progressDialog;
    public void showProgress(boolean show ){

        if( show ){

            try{
                progressDialog = new ProgressDialog( this );
                progressDialog.setCancelable(false);
                progressDialog.setMessage("잠시만 기다려주세요.");
                progressDialog.show();
            }catch(Exception e){

            }


        }else{

            if( progressDialog != null ){
                progressDialog.dismiss();
            }

        }

    }

    private String selectedAssetNo = "";
    private class DataManager extends AsyncTask {

        private DataManager() {
        }

        private int _statusCode;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("onPreExecute");
            //showProgress(true);

        }

        @Override
        protected Object doInBackground(Object[] params) {

            AsyncHttpClient client = new SyncHttpClient();
            ByteArrayEntity entity = null;

            String _url  =   OnbridPreference.getString(AssetListActivity.this, "url") + "/fileSearch";
            //String _message =   "{\"UNIVNO\":\"0000\"}";

            Map param = new HashMap();
            param.put("UNIVNO"           , OnbridPreference.getString(AssetListActivity.this, "univNo"));
            param.put("CAMPUSNO"         , OnbridPreference.getString(AssetListActivity.this, "campusNo"));
            param.put("CATEGORY"         , "images");
            param.put("CATEGORYITEM"     , selectedAssetNo);
            param.put("LOGINEMP"         , OnbridPreference.getString(AssetListActivity.this, "loginEmp"));
            // Log.e("closeItem.param", param.toString());

            String _message = new Gson().toJson(param);

            final ArrayList<Map> data = new ArrayList<Map>();

            try{



                entity = new ByteArrayEntity( _message.getBytes("UTF-8") );

                client.post( getParent(), _url, entity, "application/json", new JsonHttpResponseHandler(){
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);

                        try{

                            JSONObject item;
                            for( int i=0 ; i<response.length() ; i++ ){
                                item = (JSONObject) response.get(i);

                                Log.e("item", item.getString("URL").toString());

                                data.add((Map) (new HashMap()).put("URL", item.getString("URL").toString()));
                            }

                        }catch (Exception e){

                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                    }
                });


            }catch (Exception e){

            }

            return data;
        }



        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            files = (ArrayList<Map>) o;

            showProgress(false);
            loading = false;

        }
    }
}
